//
//  CustomGalleryPicker.swift
//  Template
//
//  Created by MacBook on 1/23/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit
import Gallery
import MediaPlayer
import AVKit
import Lightbox



protocol CustomGalleryPickerDelegate {
    func sendImageSelection(image: [UIImage]?)
    func sendVideoSelction(videoURL: URL?)
}


class CustomGalleryPicker: NSObject {

    static let shared = CustomGalleryPicker()
    var delegates: CustomGalleryPickerDelegate?
    
    override init() {
        
    }
    
    var viewController = UIViewController()
    
    /*
     // MARK: - Open Library
     */
    
    func openPhotoLibrary()
    {
        let gallery = GalleryController()
        gallery.delegate = self
        viewController.present(gallery, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Setup Configuration
     */
    func setupConfigutaion()
    {
        Config.Permission.image = #imageLiteral(resourceName: "tab_02")
        Config.Font.Text.bold = UIFont(name: "Arial", size: 14)!
        Config.Camera.recordLocation = true
        Config.tabsToShow = [.imageTab, .cameraTab]
        
        // configution
        Config.VideoEditor.maximumDuration = 30
        Config.VideoEditor.savesEditedVideoToLibrary = true
    }
    
    
    /*
     // MARK: - Light Box
     */
    
    func showLightbox(images: [UIImage]) {
        guard images.count > 0 else {
            return
        }
        
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        print(lightboxImages)
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.dismissalDelegate = self
        
        viewController.present(lightbox, animated: true, completion: nil)
    }
}

extension CustomGalleryPicker: LightboxControllerDismissalDelegate
{
    // MARK: - LightboxControllerDismissalDelegate
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension CustomGalleryPicker: GalleryControllerDelegate
{
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image])
    {
        controller.dismiss(animated: true, completion: nil)
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            
            var imagesArray = [UIImage]()
            imagesArray = resolvedImages.compactMap({ $0 })
            
            self?.delegates?.sendImageSelection(image: imagesArray)
            //self?.showLightbox(images: imagesArray )
        })
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video)
    {
        controller.dismiss(animated: true, completion: nil)
        let editor = VideoEditor()
        editor.edit(video: video) { (editedVideo: Video?, tempPath: URL?) in
            DispatchQueue.main.async {
                if let tempPath = tempPath {
                    let controller = AVPlayerViewController()
                    controller.player = AVPlayer(url: tempPath)
                    self.delegates?.sendVideoSelction(videoURL: tempPath)
                    //self.viewController.present(controller, animated: true, completion: nil)
                }
            }
        }
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image])
    {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController)
    {
        controller.dismiss(animated: true, completion: nil)
    }
}
