//
//  SocialNetworkingCalls.swift
//  Template
//
//  Created by MacBook on 1/22/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import TwitterKit

import GoogleSignIn
import LinkedinSwift

enum socialNetworkingType
{
    case isTypeFB
    case isTypeTwitter
    case isTypeGoogle
    case isTypelinkedin
    case none
}

class SocialNetworkingSingleton
{
    // MARK: - Properties
    static let shared = SocialNetworkingSingleton(tmpSoicalNetworkgin: socialNetworkingType.none)
    
    // MARK: -
    var socialnetworkingType: socialNetworkingType
    var picture: String = ""
    var name: String = ""
    var user_name: String = ""
    var socialId: String = ""
    var email: String = ""
    
    // Initialization
    private init(tmpSoicalNetworkgin: socialNetworkingType)
    {
        self.socialnetworkingType = tmpSoicalNetworkgin
        self.picture = kEmptyString
        self.name = kEmptyString
        self.user_name = kEmptyString
        self.socialId = kEmptyString
        self.email = kEmptyString
    }
}

protocol SocialNetworkingCompltionHandlerDelegate {
    func sendFacebookData(singltn: SocialNetworkingSingleton?)
    func sendTwitterData(singltn: SocialNetworkingSingleton?)
    func sendGoogleData(singltn: SocialNetworkingSingleton?)
    func sendLinkdeinObject(singltn: SocialNetworkingSingleton?)
}

protocol SocialNetworkingCompletionHandler {
    
    var singleTon: SocialNetworkingSingleton? {get set}
}

class SocialNetworkingCalls: NSObject, SocialNetworkingCompletionHandler
{
    var singleTon: SocialNetworkingSingleton?
    
    var delegate: SocialNetworkingCompltionHandlerDelegate?

    static let shared = SocialNetworkingCalls()
    
    // Initialization
    private override init()
    {
        
    }
    
    var viewController: UIViewController?
    
    // MARK: - Google
    func googleLogin()
    {
        self.singleTon = SocialNetworkingSingleton.shared
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    // MARK: - Twitter Login
    func twitterLogin()
    {
        self.singleTon = SocialNetworkingSingleton.shared
        
        // Swift
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                
                self.singleTon?.socialnetworkingType = .isTypeTwitter
                printObject(objectPrint: "signed in as \(session?.userName ?? kEmptyString)")
                
                self.singleTon?.socialId = (session?.userID)!
                self.singleTon?.user_name = (session?.userName)!
                
                self.getEmail()
            } else {
                printObject(objectPrint:("error: \(error?.localizedDescription ?? kEmptyString)"));
            }
        })
    }
    
    
    func getEmail()
    {
        // Swift
        let client = TWTRAPIClient.withCurrentUser()
        
        client.requestEmail { email, error in
            if (email != nil) {
                
                self.singleTon?.email = email!
                
                self.delegate?.sendTwitterData(singltn: self.singleTon)
                
            } else {
                printObject(objectPrint: "error: \(error?.localizedDescription ?? kEmptyString)");
            }
        }
    }
    
    // MARK: - Fb Login
    func fbLogins()
    {
        self.singleTon = SocialNetworkingSingleton.shared
        
        if(viewController == nil)
        {
            showToast(view: (appDelet.window?.rootViewController?.view)!, message: kAPiNotInitize)
            return
        }
        
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions:[ .publicProfile, .email, .userFriends ], viewController: self.viewController) { loginResult in
            switch loginResult {
            case .failed(let error):
                printObject(objectPrint: error)
            case .cancelled:
                printObject(objectPrint:"User cancelled login.")
            case .success( _, _, _):
                printObject(objectPrint:"Logged in!")
                self.getFBUserData()
            }
        }
    }
    
    //function is fetching the user data
    func getFBUserData(){
        
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    
                    let dict = result as! [String : AnyObject]
                    printObject(objectPrint:(result!))
                    printObject(objectPrint:(dict))
                  
                    let picutreDic = dict as NSDictionary
                    let tmpURL1 = picutreDic.object(forKey: "picture") as! NSDictionary
                    let tmpURL2 = tmpURL1.object(forKey: "data") as! NSDictionary
                    let finalURL = tmpURL2.object(forKey: "url") as! String
                    self.singleTon?.picture = finalURL
                    
                    self.singleTon?.socialnetworkingType = .isTypeFB
                    
                    let nameOfUser = picutreDic.object(forKey: "name") as! String
                    self.singleTon?.name = nameOfUser
                    
                    var tmpEmailAdd = ""
                    if let emailAddress = picutreDic.object(forKey: "email"){
                        tmpEmailAdd = emailAddress as! String
                    }
                    else
                    {
                        var usrName = nameOfUser
                        usrName = usrName.replacingOccurrences(of: " ", with: "")
                        tmpEmailAdd = usrName+"@facebook.com"
                    }
                    self.singleTon?.email = tmpEmailAdd
                    
                    let fbId = picutreDic.object(forKey: "id") as! String
                    self.singleTon?.socialId = fbId
                    
                    self.delegate?.sendFacebookData(singltn: self.singleTon)
                }
            })
        }
    }
    
    // MARK: - Linkedin
    func loginWithLinkedin() {
        
        self.singleTon = SocialNetworkingSingleton.shared
        let config = LinkedinSwiftConfiguration(clientId: linkedIn_clientId, clientSecret: linkedIn_clientSecret, state: linkedIn_clientState, permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: linkedIn_clientRedirectURL)
        
        let linkedinHelper = LinkedinSwiftHelper(configuration: config!)
        
        let wholeURL = linkedIn_apiURL
        //"https://api.linkedin.com/v1/people/~?format=json"
        
        linkedinHelper.authorizeSuccess({ (lsToken) -> Void in
            //Login success lsToken
            
            linkedinHelper.requestURL(wholeURL,
                                      requestType: LinkedinSwiftRequestGet,
                                      success: { (response) -> Void in
                                        
                                        let dinctyObject = response.jsonObject! as NSDictionary
                                        let firntName = dinctyObject.object(forKey: "firstName") as! String
                                        self.singleTon?.name = firntName
                                        let idss = dinctyObject.object(forKey: "id") as! String
                                        
                                        let formattedName = dinctyObject.object(forKey: "formattedName") as! String
                                        self.singleTon?.socialId = idss
                                        
                                        self.singleTon?.user_name = formattedName
                                        
                                        let tmpEmailAdd = dinctyObject.object(forKey: "emailAddress") as! String
                                        self.singleTon?.email = tmpEmailAdd
                                        
                                        let pictureUrl = dinctyObject.object(forKey: "pictureUrl") as! String
                                        self.singleTon?.picture = pictureUrl
                                        
                                        self.singleTon?.socialnetworkingType = .isTypelinkedin
                                        printObject(objectPrint: response.jsonObject)

                                        self.delegate?.sendLinkdeinObject(singltn: self.singleTon)
                                        
                                        //Request success response
                                        
            }) { [unowned self] (error) -> Void in
                
                printObject(objectPrint: self)
                //Encounter error
            }
            
            
        }, error: { (error) -> Void in
            //Encounter error: error.localizedDescription
        }, cancel: { () -> Void in
            //User Cancelled!
        })
    }
}


extension SocialNetworkingCalls : GIDSignInUIDelegate ,GIDSignInDelegate
{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        printObject(objectPrint: user)
        
        if(user != nil)
        {
            self.singleTon?.socialnetworkingType = .isTypeGoogle
            //
            
            self.singleTon?.socialId = user.userID
            self.singleTon?.user_name = user.profile.familyName
            self.singleTon?.email = user.profile.email
            
            if user.profile.hasImage
            {
                let pic = user.profile.imageURL(withDimension: 100)
                self.singleTon?.picture = (pic?.absoluteString)!
            }
            
            self.delegate?.sendGoogleData(singltn: self.singleTon)
        }
    }
    
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        
        //myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        
        if(self.viewController == nil)
        {
            showToast(view: (appDelet.window?.rootViewController?.view)!, message: kAPiNotInitize)
            return
        }
        
        self.viewController?.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        
        self.viewController?.dismiss(animated: true, completion: nil)
    }
}
