//
//  UIColor+Extension.swift
//  TheMall
//
//  Created by Nouman Gul Junejo on 1/10/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com All rights reserved.
//

import UIKit

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return start - end
    }
}
