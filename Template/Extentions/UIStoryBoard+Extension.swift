//
//  UIStoryBoard+Extension.swift
//  Free Agent
//
//  Created by Nouman Gul Junejo on 1/22/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com All rights reserved.
//

import UIKit

enum Main : String {
    case InitialNavigationController = "InitialNavigationController",
    LoginOptionsController = "LoginOptionsController",
    LoginController = "LoginController", 
    ForgotPasswordController = "ForgotPasswordController",
    SignUpController = "SignUpController",
    StartController = "StartController"
}

extension UIStoryboard {
    static func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}

