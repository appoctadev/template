//
//  FormValidation.swift
//  Template
//
//  Created by MacBook on 1/28/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit

class FormValidation: NSObject {

    static func actionButtonformValidation(name: String, email: String, phoneNo: String, password: String, completion: (_ result: Bool, _ phoneFormat: String) -> Void)
    {
        var message = kEmptyString
        var validForm = true
        
        let format = phoneNo.format()
        
        if name.simpleNameValidation == false
        {
            message += name + kFieldIsEmpty
            validForm = false
            message += amparcent
        }
        
        if actionButtonFormValid(name: name)
        {
            
        }
        
        if actionButtonFormValid(name: email)
        {
            if email.isEmail == false
            {
                message = kEmailNotValid
                message += amparcent
                validForm = false
            }
        }
        
        if actionButtonFormValid(name: phoneNo)
        {
            if phoneNo.phoneNumber == false
            {
                message += kPhoneNotVaild
                message += amparcent
                validForm = false
            }
        }
        
        if actionButtonFormValid(name: password)
        {
            if password.isValidPassword == false
            {
                message += kPasswordNotValid
                message += amparcent
                validForm = false
            }
        }
        
        if(validForm == false)
        {
            let messages = message.dropLast(3)
            showToast(view: (appDelet.window?.rootViewController?.view)!, message: String(messages))
        }
        
        
        
        completion(validForm, format ?? kEmptyString)
    }
    
    static func actionButtonFormValid(name: String)  -> Bool
    {
        if name.simpleNameValidation == false
        {
            let essage = name + kFieldIsEmpty
            showToast(view: (appDelet.window?.rootViewController?.view)!, message: essage)
            return false
        }
        
        return true
    }
}
