//
//  Helper.swift
//  HopTheLine
//
//  Created by Nouman Gul Junejo on 7/17/17.
//  Copyright © 2017 Nouman Gul Junejo All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import RMMapper


class Helper {
    
}

extension Helper {

    //user Methods
    
    static func getUser() -> User?  {
        if let user = UserDefaults.standard.rm_customObject(forKey: "user") {
            return DataHandler.sharedInstance.mapUserResponse(responseData: user as AnyObject)
        }
        return nil
    }
    
    static func saveUser(user : AnyObject) {
        UserDefaults.standard.rm_setCustomObject(user, forKey: "user")
    }
    
    static func removeUser() {
        UserDefaults.standard.removeObject(forKey: "user")
    }
    
    //

    //check login user type here.. busybee or user
    static func isLoginTypeUser(completionHandler:@escaping (Bool) -> ()) {
        
        let user = getUser()
        
        if user?.user_type == kBusyBee {
            completionHandler(true)
        }else{
            completionHandler(false)
        }
    }
    
    static func stringIntoDate(dateString:String,dateFormat:String )->Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let myDate = dateFormatter.date(from: dateString)!
        dateFormatter.timeZone = TimeZone.current
        return myDate
    }
 
    static func getDeviceType() -> ScreenType{
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4
        case 1136:
            return .iPhone5
        case 1334:
            return .iPhone6
        case 2208:
            return .iPhone6Plus
        case 2436:
            return .iPhoneX
        default:
            return .Unknown
        }
    }
}
enum ScreenType: String {
    case iPhone4
    case iPhone5
    case iPhone6
    case iPhone6Plus
    case iPhoneX
    case Unknown
}
