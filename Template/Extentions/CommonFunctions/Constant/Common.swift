//
//  Constants.swift
//  GainSafe
//
//  Created by Umar.
//  Copyright © 2017 Digitonics. All rights reserved.
//

import Foundation
import UIKit
import TTGSnackbar
import AVFoundation
import Alamofire
import Lottie
//import Toast_Swift

// MARK: - Shared Application Object
let appDeletForBadee = UIApplication.shared
let appDelet = UIApplication.shared.delegate as! AppDelegate

/*
// MARK: - UserDefaults
*/
func saveUsrDefault(keyValue: String, valueIs: String)
{
    let defaults = UserDefaults.standard
    defaults.synchronize()
    UserDefaults.standard.synchronize()
    defaults.set(keyValue, forKey: valueIs)
}


func setDefaultValue(keyValue: String, valueIs: String)
{
    let usrdefault = UserDefaults.standard
    usrdefault.synchronize()
    usrdefault.set(valueIs, forKey: keyValue)
    usrdefault.synchronize()
}

func getValueForKey(keyValue: String) -> String
{
    let defaults = UserDefaults.standard
    defaults.synchronize()
    UserDefaults.standard.synchronize()
    
    let valueForStr = defaults.value(forKey: keyValue)
    if(valueForStr != nil)
    {
        return valueForStr as! String
    }
    else{
        return ""
    }
}

/*
 // MARK: - ResizeImage
 */
func RBResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
    let size = image.size
    
    let widthRatio  = targetSize.width  /  image.size.width
    let heightRatio = targetSize.height / image.size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio) //CGSizeMake(size.width * heightRatio, size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        //CGSizeMake(size.width * widthRatio,  size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height) //CGRectMake(0, 0, newSize.width, newSize.height)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!
}

/*
 // MARK: - Aleert & Snack Bar
 */
func showAlert(title: String, message: String, viewController: UIViewController) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(okAction)
    viewController.present(alertController, animated: true, completion: nil)
}

func showToast(view: UIView, message: String)
{
    view.endEditing(true)
    
    let snackbar = TTGSnackbar(message: message, duration: .short)
    
    snackbar.show()
}

/*
 // MARK: - LogoutUser
 */
func logoutUser()
{
    var uUID = UIDevice.current.identifierForVendor!.uuidString
    
    if(getValueForKey(keyValue: kSaveUDID) == kEmptyString)
    {
        saveUsrDefault(keyValue: uUID, valueIs: kSaveUDID)
    }
    else
    {
        uUID = getValueForKey(keyValue: kSaveUDID)
    }
    
   APIClient.LogoutAPI(udid: uUID) { (usrObject, error, message) in
    
    }
    
    
}

/*
 // MARK: - Convert Notifiction json key to Object
 */
func convertToDictionary(text: String) -> [String: Any]? {
    do{
        if let json = text.data(using: String.Encoding.utf8){
            if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject]{
                
                //let id = jsonData["id"] as! String
                return jsonData
            }
        }
    }catch {
        printObject(objectPrint:error.localizedDescription)
        
    }
    return nil
}

/*
 // MARK: - Convert Time via UTC
 */
func UTCToLocal(date:String, serverFormat: String, convertedFormat: String) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = serverFormat //"H:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = convertedFormat //"h:mm a"
    
    if(dt != nil)
    {
        return dateFormatter.string(from: dt!)
    }
    return kEmptyString
}

func UTCTOLocalDate (date:String, serverFormat: String, convertedFormat: String) -> Date
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = serverFormat //"H:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    
    var dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = convertedFormat //"h:mm a"
    
    if(dt != nil)
    {
        let date1 = dateFormatter.string(from: dt!)
        return dateFormatter.date(from: date1)!
    }
    else
    {
        dateFormatter.dateFormat = convertedFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        dt = dateFormatter.date(from: date)
        //let date1 = dateFormatter.string(from: dt!)
        //return dateFormatter.date(from: date1)!
        return Date.init()
    }
}

func getCurrentDateObject(serverFormat: String, convertedFormat: String) -> Date
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = serverFormat //"H:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let daetValue = Date.init()
    let date = dateFormatter.string(from: daetValue)
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = convertedFormat //"h:mm a"
    
    if(dt != nil)
    {
        let date1 = dateFormatter.string(from: dt!)
        return dateFormatter.date(from: date1)!
    }
    return Date.init()
}

func getCurentDateAndTime(serverFormat: String, convertedFormat: String)-> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = serverFormat //"H:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let daetValue = Date.init()
    let date = dateFormatter.string(from: daetValue)
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone.current
    dateFormatter.dateFormat = convertedFormat //"h:mm a"
    
    if(dt != nil)
    {
        return dateFormatter.string(from: dt!)
    }
    return kEmptyString
}


func localToUTC(date:String) -> String
{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "h:mm a"
    dateFormatter.calendar = NSCalendar.current
    dateFormatter.timeZone = TimeZone.current
    
    let dt = dateFormatter.date(from: date)
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    dateFormatter.dateFormat = "H:mm:ss"
    
    return dateFormatter.string(from: dt!)
}

func formatDateTime(date: String) -> String {
    //date input formate
    let dateAsString = date
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    //date output format
    let date = dateFormatter.date(from: dateAsString)
    dateFormatter.dateFormat = "dd MMM yyyy"
    let Date12 = dateFormatter.string(from: date!)
    
    //time output format
    dateFormatter.dateFormat = "h:mm a"
    dateFormatter.timeZone = NSTimeZone.local
    
    let time = dateFormatter.string(from: date!)
    
    let comibine = Date12 + " at " + time
    return comibine
}

func getCurrentDate() -> String {
    let date = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    formatter.timeZone = TimeZone(abbreviation: "UTC")
    
    let finalDate = formatter.string(from: date)
    return finalDate
}


func formatDate(date: String) -> String {
    //date input formate
    let dateAsString = date
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    
    //date output format
    let date = dateFormatter.date(from: dateAsString)
    dateFormatter.dateFormat = "dd MMM yyyy"
    let Date = dateFormatter.string(from: date!)
    
    return Date
}

func formatTime(date: String) -> String {
    //date input formate
    let dateAsString = date
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
    
    //date output format only time
    let date = dateFormatter.date(from: dateAsString)
    dateFormatter.dateFormat = "h:mm a"
    dateFormatter.timeZone = NSTimeZone.local
    let Time12 = dateFormatter.string(from: date!)
    return Time12
}


/*
 // MARK: - Validate Email
 */
func isValidateEmail(email:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: email)
}


func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

/*
 // MARK: - Animate TableView
 */
func animateTable(tblview: UITableView) {
    tblview.reloadData()
    
    let cells = tblview.visibleCells
    let tableHeight: CGFloat = tblview.bounds.size.height
    
    for i in cells {
        let cell: UITableViewCell = i as UITableViewCell
        cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
    }
    
    var index = 0
    
    for a in cells {
        let cell: UITableViewCell = a as UITableViewCell
        UIView.animate(withDuration: 0.8, delay: 0.05 * Double(index), options: [], animations: {
            cell.transform = CGAffineTransform(translationX: 0, y: 0);
        }, completion: nil)
        
        index += 1
    }
}


/*
 // MARK: - Loader
 */
let bgCOlor = UIColor(named: "logo_color")

let objAJProgressView = AJProgressView()

var animatedValue = AnimationView(name: kEmptyString)

struct ScreenSize  {
    static let Width         = UIScreen.main.bounds.size.width
    static let Height        = UIScreen.main.bounds.size.height
    static let Max_Length    = max(ScreenSize.Width, ScreenSize.Height)
    static let Min_Length    = min(ScreenSize.Width, ScreenSize.Height)
}

struct DeviceType {
    static let iPhone4  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.Max_Length < 568.0
    static let iPhone5_5s  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.Max_Length == 568.0
    static let iPhone6_6s_7 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.Max_Length == 667.0
    static let iPhone6P_6sP_7P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.Max_Length == 736.0
    static let iPhoneX = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.Max_Length == 812.0
    static let iPad = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.Max_Length == 1024.0
}

func intilizeAnimation()
{
    let viewWindow = (appDelet.window?.rootViewController?.view)!
    let midY = viewWindow.frame.height / 2
    let midX = viewWindow.frame.width / 2

    var widthProgressView : CGFloat {
        
        var width = CGFloat()
        if DeviceType.iPhone4 || DeviceType.iPhone5_5s{
            width = ScreenSize.Min_Length*0.2
        }else if DeviceType.iPhone6_6s_7 || DeviceType.iPhone6P_6sP_7P || DeviceType.iPhoneX{
            width = ScreenSize.Min_Length*0.25
        }else if DeviceType.iPad{
            width = ScreenSize.Min_Length*0.15
        }
        return width
    }
    
    animatedValue.frame = CGRect(x: (ScreenSize.Width - widthProgressView)/2, y: (ScreenSize.Height - widthProgressView)/2, width: widthProgressView, height: widthProgressView)
        
        //CGRect(x: midX, y: midY, width: 400, height: 400)
    
    let starAnimation = Animation.named("714-water-loader")
    animatedValue.animation = starAnimation
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    if (appDelegate.window?.subviews.contains(animatedValue))! {
        appDelegate.window?.bringSubviewToFront(animatedValue)
        printObject(objectPrint:"already there")
    }else{
        appDelegate.window?.addSubview(animatedValue)
        appDelegate.window?.bringSubviewToFront(animatedValue)
    }
    
    
}

func isShowIndicator(showIndica: Bool)
{
    // Pass your image here which will come in centre of ProgressView
    /*objAJProgressView.imgLogo = UIImage(named:"logo_icon")!
    
    // Pass the colour for the layer of progressView
    objAJProgressView.firstColor = bgCOlor
    
    // If you  want to have layer of animated colours you can also add second and third colour
    objAJProgressView.secondColor = bgCOlor
    objAJProgressView.thirdColor = bgCOlor
    
    // Set duration to control the speed of progressView
    objAJProgressView.duration = 3.0
    
    // Set width of layer of progressView
    objAJProgressView.lineWidth = 8.0
    
    //Set backgroundColor of progressView
    objAJProgressView.bgColor =  UIColor.black.withAlphaComponent(0.2)
    
    
    if(showIndica == true)
    {
        objAJProgressView.show()
    }
    else
    {
        objAJProgressView.hide()
    }*/
    
    if(showIndica)
    {
        animatedValue.play { (finished) in
            /// Animation stopped
            
            if(finished)
            {
                showAnimation()
            }
        }
    }
    else{
//        animatedValue.pause()
//        animatedValue.removeFromSuperview()
    }
}

func showAnimation()
{
    animatedValue.play { (finished) in
        /// Animation stopped
        
        if(finished)
        {
            showAnimation()
        }
    }
}

/*
 // MARK: - Validate Erros
 */
let REGEX_EMAIL = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
let REGEX_USER_NAME = "[[._-a-zA-Z0-9 ]]{3,20}$"

enum ValidationErrors : String {
    case Email = "Enter Valid Email Address.",
    NameLimit = "should be 3 - 15 characters long.",
    //    Password = "Password should be 8-20 characters long, contains atleast one upper case alphabet and digit.",
    Password = "Enter Valid Password.",
    PasswordsMatchingError = "Passwords donot match.",
    TermsAndConditions = "Please accept Terms and Conditions to sign up.",
    FreindRequest = "Your friend request has been sent.",
    AcceptedRequest = "Friend Request Accepted.",
    CancelRequest = "Friend Request Cancelled.",
    UnfriendUser = "Updated successfully."
}


func isValidData(_ input:String,regexPattern : String) -> Bool{
    //
    do {
        if regexPattern != ""{
            let regex = try NSRegularExpression(pattern: regexPattern, options: NSRegularExpression.Options.caseInsensitive)
            if regex.firstMatch(in: input, options: [], range: NSRange(location: 0, length: input.utf16.count)) != nil {
                return true
            }
        }else{
            return true
        }
    } catch {
        // regex was bad!
    }
    return false
}


/*
 // MARK: - Create Thumbnail
 */
func createThumbnailImg(videoURL: URL) -> UIImage
{
    var thumanilImage = UIImage()
    let asset = AVAsset(url: videoURL)
    let durationSeconds = CMTimeGetSeconds(asset.duration)
    let generator = AVAssetImageGenerator(asset: asset)
    
    generator.appliesPreferredTrackTransform = true
    
    let time = CMTimeMakeWithSeconds(durationSeconds/3.0, preferredTimescale: 600)
    generator.generateCGImagesAsynchronously(forTimes: [NSValue.init(time: time) ]) { (requestedTime, thumbnail, actualTime, result, error) in
        
        DispatchQueue.main.async {
            let usrPic = UIImage(cgImage: thumbnail!)
            thumanilImage =  usrPic
        }
    }
    
    return thumanilImage
}

/*
 // MARK: - Paramter
 */

func convertKeyValueToParameter(objectArray: [String: String]) -> Parameters
{
    var parmeterObject = Parameters()
    parmeterObject = objectArray
    
    printObject(objectPrint:parmeterObject)
    
    return parmeterObject
}


/*
 // MARK: - PrintObject
 */

func printObject(objectPrint: Any)
{
    #if DEBUG
        debugPrint("responseData postRequest ==\(objectPrint))")
        print(objectPrint)
    #endif
}
