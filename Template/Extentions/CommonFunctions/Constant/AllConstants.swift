//
//  AllConstants.swift
//  Template
//
//  Created by MacBook on 1/18/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit

/*
 // MARK: - Storyboards
 */
let kMainStoryboard = "Main"
let kStartStoryboard = "Start"
let kHomeStoryboard = "Home"

//MARK:- Navigation titles
let kTitleInspectionType = "Inspection Type"


//MARK:- Theme Colors
let kTabbarColor = UIColor(red: 0/255, green: 183/255, blue: 173.0/255, alpha: 1.0)
let kNavBarButtonColor = UIColor(red: 37/255, green: 77/255, blue: 100/255, alpha: 1.0)
let kSelectedCellColor = UIColor(red: 247/255, green: 70/255, blue: 72/255, alpha: 1.0)
let kAppLightBlueColor = UIColor(red: 78/255, green: 135/255, blue: 167/255, alpha: 1.0)
let kSaveReportColor = UIColor(red: 208/255, green: 216/255, blue: 225/255, alpha: 1.0)

let kRedAppColor = UIColor(red: 247/255, green: 68/255, blue: 69/255, alpha: 1)
let kGreenAppColor = UIColor(red: 138/255, green: 224/255, blue: 131/255, alpha: 1)

let kDarkBlueAppColor = UIColor(red: 37/255, green: 77/255, blue: 100/255, alpha: 1)
let kLightBlueAppColor = UIColor(red: 98/255, green: 151/255, blue: 179/255, alpha: 1)


/*
 // MARK: - Notification Identifiers
 */
struct UpdateUserNotification {
    static let UserUpdate = NSNotification.Name("UserUpdate")
}

struct ShowPopUp {
    static let DepartMenu = NSNotification.Name("ShowDepartMenu")
    static let SaveReportMenu = NSNotification.Name("ShowSaveReportMenu")
    static let SubmitReportMenu = NSNotification.Name("ShowSubmitReportMenu")
}

struct NotificationCounter{
    static let NotificationIncrease = NSNotification.Name("NotificationIncrease")
    static let NotificationDecrease = NSNotification.Name("NotificationDecrease")
}

//MARK:- Enums

enum Filters: String {
    case Featured = "Featured"
    case TopRated = "Top Rated"
    case MostViewed = "Most Viewed"
    case RecentViewed = "Recently Viewed"
    case Audio = "Audio"
    case Video = "Video"
}

enum ServiceTypes: String {
    case Vehicle = "vehicle"
    case Driver = "recruitment"
    case Merchandise = "merchandise"
    case Website = "website"
    case Home = "home"
}

enum MediaType: String {
    case Video = "2"
    case Audio = "1"
}


//MARK:- Constants
let kAPIErrorMessage = "Something went wrong"

let KNotesVechileString = "By Clicking submit, I understand that this order cannot be cancelled. Modifications can be made within 30 minutes of submission request by calling"
let KNotesWebsiteString = "Note: Business will not be activated until our Busy Bee Verifier has verified all information."
let KNotesEquipmentString = "By Clicking submit, I understand that this order cannot be cancelled. Modifications can be made within 30 minutes of submission request by calling"
let KNotesHomeString = "By Clicking submit, I understand that this order cannot be cancelled. Modifications can be made within 30 minutes of submission request by calling"

let kVerifyAndAddPHoto = "Please attach all pictures in order to proceed"
let kAddComment = "Please enter some comments"

let kAgentStatusOnline = "online"
let kAgentStatusOffline = "offline"
let kAgentStatusBusy = "busy"

let kPushNotifictionNotSetup = "Kindly setup push notifiction in order to implement"
let kFirebasePushFilePlist = "GoogleService-Info"

let kIsReqeustStatusPending = "pending"
let kIsRequestTypeProgress = "progress"
let kIsReqeustStatusVerifed = "verified"
let kIsReqeustStatusShouldVerifed = "upload Report"
let kIsReqeustStatusUnassign = "not assigned"

let kIsRequestedPreviously = "Your reqeust is already in progress once its done then you will send another reqeust."

let kStatusNotChange = "You can't change your status as you are engage in your job"
let kNoMoreRequest = "No More request at this time please try later"

let kReqeustAccept = "accept"
let kReqeustReject = "reject"

let kResendReqeust = "Not Assigned Yet! Click to Resend"

let kMyInpectionConstant = "My Inspections"
let kMyInpectionOtherConstant = "Notifications"

let kPushNotificaitonTypeAddReqeust = "add_request"
let kPushNotificaitonTypeAcceptReqeust = "accept_request"
let kPushNotificaitonTypeReqeustVerified = "submission"

let kTopNavigationVechile = "Vehicle Inspection"
let kTopNavigationHouse = "Home Inspection"
let kTopNavigationEquipment = "Equipment Inspection"
let kTopNavigationWebsite = "Website Verification"

let unableToConnect = "Oops! something went wrong. Please try again."

let kfifteenMin = "15"
let middle = "You can resend your request after "
let last = " minutes"

let kNotResendApi = middle + kfifteenMin + last

let kNotavablibleString = "N/A"

let kSaveUDID = "saveUDID"
let kUserNotifictionCoutner = "notifiitonCounter"
let kNotificationCounter = "counterofnotiiction"
let kUpdateLocation = "locationUdpatesValue"

let kMapAndRequestNotificaion = "mapNotifictionView"


let kdeviceToken = "deviceTokens"
let kEmptyString = ""
let kurname = "usrame"
let kpaswrd = "pasword"
let kNotSelected = "0"
let kSegueBBVechileLocationViewController = "BBVechileLocationViewController"
let kBusyBee = ""

let kAPiNotInitize = "Kindly initize view controller/ api key so that it will work accordingly"

let kFieldIsEmpty = "field is empty"
let kEmailNotValid = "not valid email"
let kPhoneNotVaild = "phone is not valid"
let kPasswordNotValid = "Password is not valid"

let amparcent = " & "
//MARK :- Images Constant

let kImgOnline = "btn_onlinej"
let kNoDataFound = "img_data_not_found"

