//
//  GoogleMapsWrapper.swift
//  Template
//
//  Created by MacBook on 1/28/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit
import GoogleMaps

class GoogleMapsWrapper: NSObject {
    
    static let shared = GoogleMapsWrapper()
    var delegte: PlacesGoogleLocationDelegate?
    
    override init() {
        
    }
    
    func actionButtonShowMap(viewCon: UIViewController)
    {
        let nbViews = PlacesGoogleLocation.instantiateFromNib()
        nbViews?.checkGoogleServices(viewCOn: viewCon)
        nbViews?.delegte = self
        viewCon.view.addSubview(nbViews!)

//        customView.delegte = self
     //   customView.callAndShowMap()
    }
}

extension GoogleMapsWrapper: PlacesGoogleLocationDelegate
{
    /// Media Launched successfully on the cast device
    func getLaitude(latitude: String, longitdue: String, address: String, city: String, state: String, marker: GMSMarker?)
    {
        self.delegte?.getLaitude(latitude: latitude, longitdue: longitdue, address: address, city: city, state: state, marker: marker)
    }
}


