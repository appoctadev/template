//
//  PlacesGoogleLocation.swift
//  Template
//
//  Created by MacBook on 1/28/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit

import GoogleMaps
import UIKit
import GooglePlaces

protocol PlacesGoogleLocationDelegate: class {
    
    /// Media Launched successfully on the cast device
    func getLaitude(latitude: String, longitdue: String, address: String, city: String, state: String, marker: GMSMarker?)
}



class PlacesGoogleLocation: UIView {

    let locationManager = CLLocationManager()
    
    var delegte: PlacesGoogleLocationDelegate?
    
    var address: String = kEmptyString
    var city: String = kEmptyString
    var state: String = kEmptyString
    
    let selectedColor = UIColor(named: "logo_color")
    
    @IBOutlet weak var mapView: GMSMapView!
    var locationOfPinPionter = CLLocation()
    
    @IBOutlet weak var btnOk: UIButton!
    
    @IBOutlet weak var txtfieldStreetName: UITextField!
    var viewControler: UIViewController?


    
    func checkGoogleServices(viewCOn: UIViewController)
    {
        self.viewControler = viewCOn
        self.mapView.delegate = self
        self.mapView?.isMyLocationEnabled = true
        
        self.mapView?.settings.myLocationButton = true
        
        self.mapView?.settings.setAllGesturesEnabled(true)
        
        // For use in foreground
        self.locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            
            switch(CLLocationManager.authorizationStatus())
            {
            case .notDetermined, .restricted, .denied:
                print("No access")
                self.openSetting()
                break
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                break
                
            default:
                print("...")
                
                break
            }
        }
        else
        {
            print("Location services are not enabled")
            openSetting()
        }
    }
    
    func openSetting()
    {
        let alert = UIAlertController(title: "BusyBee", message: "GPS access is restricted. In order to use tracking, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Go to Settings now", style: .default, handler: { (alert: UIAlertAction!) in
            UIApplication.shared.open(NSURL(string:UIApplication.openSettingsURLString)! as URL, options: [ : ], completionHandler: { (booltes) in
                
            })
        }))
    }
    
    @IBAction func actionBUttonDone(sender: UIButton)
    {
        let latitudes = String(self.locationOfPinPionter.coordinate.latitude)
        let longitudes = String(self.locationOfPinPionter.coordinate.longitude)
        
        let cord2D = CLLocationCoordinate2D(latitude: (self.locationOfPinPionter.coordinate.latitude), longitude: (self.locationOfPinPionter.coordinate.longitude))
        
        let marker = GMSMarker()
        marker.position =  cord2D
        marker.title = "Location"
        marker.snippet = address
        
        self.delegte?.getLaitude(latitude: latitudes, longitdue: longitudes, address: self.txtfieldStreetName.text!, city: self.city , state: self.state, marker: marker)
        self.viewControler?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func actionButtonPlaces()
    {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.viewControler?.present(autocompleteController, animated: true, completion: nil)
    }
}


extension PlacesGoogleLocation: CLLocationManagerDelegate
{
    // MARK: - Location Manager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        if CLLocationManager.locationServicesEnabled()
        {
           // let location = locations.first!
            
            //self.locationManager.stopUpdatingLocation()
            //self.mapView.camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 15)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        
    }
}



extension PlacesGoogleLocation: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        printObject(objectPrint: ("Place name: \(place.name)"))
        printObject(objectPrint: "Place address: \(place.formattedAddress ?? kEmptyString)")
//        printObject(objectPrint: "Place attributions: \(place.attributions ?? kEmptyString)")
        
        self.viewControler?.dismiss(animated: true, completion: nil)
       
        self.address = place.formattedAddress!
        self.mapView.clear()
        
        self.txtfieldStreetName.text = place.name
        
        self.locationOfPinPionter =  CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        
        let cord2D = CLLocationCoordinate2D(latitude: (place.coordinate.latitude), longitude: (place.coordinate.longitude))
        
        if let city = place.addressComponents?.filter({(($0.type.contains("administrative_area_level_2")))}).first?.name
        {
            self.city = city
        }
        
        if let state = place.addressComponents?.filter({(($0.type.contains("administrative_area_level_1")))}).first?.name
        {
            self.state = state
        }
        
        let marker = GMSMarker()
        marker.position =  cord2D
        marker.title = "Location"
        marker.snippet = address
        
        let markerImage = UIImage(named: "pin")!
        let markerView = UIImageView(image: markerImage)
        marker.iconView = markerView
        marker.map = self.mapView
        
        self.mapView.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 15)
        
        self.btnOk.isEnabled = true
        
        self.btnOk.backgroundColor = selectedColor
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        printObject(objectPrint: ("Error: ", error.localizedDescription))
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.viewControler?.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
}

extension PlacesGoogleLocation: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        // Custom logic here
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D)
    {
        self.mapView.clear()
        
        let latLong = String(coordinate.latitude)+","+String(coordinate.longitude)
        let urlValue = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latLong+"&key="+GoogleMapsAPIKey
        
        APIClient.geoCodeReverseAPI(urlString: urlValue) { (models, error, message) in
            
            let modelObject = models?.resultArray
            if(modelObject!.count > 0)
            {
                let objectvlaue = modelObject![0]
                self.address = objectvlaue.formatted_address!
                self.mapView.clear()
                
                self.txtfieldStreetName.text = objectvlaue.formatted_address
                
                if let city = objectvlaue.address_components?.filter({(($0.types?.contains("administrative_area_level_2"))!)}).first?.long_name
                {
                    self.city = city
                }
                
                if let state = objectvlaue.address_components?.filter({(($0.types?.contains("administrative_area_level_1"))!)}).first?.long_name
                {
                    self.state = state
                }
                
                
                /* if let zip = objectvlaue.address_components?.filter({(($0.types?.contains("locality"))!)}).first?.long_name
                 {
                 
                 }*/
                
                self.locationOfPinPionter =  CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                
                let cord2D = CLLocationCoordinate2D(latitude: (coordinate.latitude), longitude: (coordinate.longitude))
                
                let marker = GMSMarker()
                marker.position =  cord2D
                marker.title = "Location"
                marker.snippet = self.address
                
                let markerImage = UIImage(named: "pin")!
                let markerView = UIImageView(image: markerImage)
                marker.iconView = markerView
                marker.map = self.mapView
                
                self.mapView.camera = GMSCameraPosition.camera(withTarget: cord2D, zoom: 15)
                
            }
        }
        // print(urlValue)
    }
}
