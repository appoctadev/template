//
//  LocationHelper.swift
//
//  Created by mac on 12/11/17.
//  Copyright © 2017 Appocta. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

/// A custom delgate method to access the location events.
protocol LocationHelperDelegate {
    func locationEnabled(latitude: CLLocationDegrees, longitude: CLLocationDegrees)
}

class LocationHelper: NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance: LocationHelper = LocationHelper()
    
    var delegate: LocationHelperDelegate!
    
    var locationManager: CLLocationManager!
    
    /// A method to determine the location.
    func determineCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            printObject(objectPrint:locationManager.location?.coordinate.latitude ?? "")
            
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                break
            case .authorizedAlways, .authorizedWhenInUse:
                break
            }
        }
    }
    
    /// A method to check location authorization change activity.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            //            manager.requestWhenInUseAuthorization()
            manager.requestAlwaysAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                
                if self.delegate != nil {
                    if locationManager.location?.coordinate.latitude != nil && locationManager.location?.coordinate.longitude != nil {
                        self.delegate.locationEnabled(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!)
                    } else {
                        
                        self.delegate.locationEnabled(latitude: 24.8615, longitude: 67.0099)
                    }
                }
            }
            
            
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            if self.delegate != nil {
                
                if locationManager.location?.coordinate.latitude != nil && locationManager.location?.coordinate.longitude != nil {
                    
                    self.delegate.locationEnabled(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!)
                }
            }
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }
    
    
    /// Purpose: Tells the delegate that new location data is available.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation: CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        manager.stopUpdatingLocation()
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        printObject(objectPrint:center)
    }
}
