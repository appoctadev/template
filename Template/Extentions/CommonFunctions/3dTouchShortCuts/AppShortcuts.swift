//
//  AppShortcuts.swift
//  BusyBee
//
//  Created by MacBook on 1/16/19.
//  Copyright © 2019 NGJ. All rights reserved.
//

import UIKit


class AppShortcut : UIMutableApplicationShortcutItem {
    var segue:String
    
    init(type:String, title:String, icon:String, segue:String) {
        self.segue = segue
        let translatedTitle = NSLocalizedString(title, comment:title)
        let iconImage = UIApplicationShortcutIcon(templateImageName: icon)
        super.init(type: type, localizedTitle:translatedTitle, localizedSubtitle:nil, icon:iconImage, userInfo:nil)
    }
}

var images:[String] = ["Vehicle","Website","Home","Merchandies"]

enum ShortcutIdentifier: String {
    
    case isTypeWebsite
    case isTypeVechil
    case isTypeHouse
    case isTypeEqupment
    case isTypePast
    
    // MARK: Initializers
    init?(fullNameForType: String) {
        guard let last = fullNameForType.components(separatedBy: ".").last else { return nil }
        
        self.init(rawValue: last)
    }
    
    // MARK: Properties
    var type: String {
        return Bundle.main.bundleIdentifier! + ".\(self.rawValue)"
    }
}

class AppShortcuts {
    
    static var shortcuts:[AppShortcut] = []
    
    
    class func sync() {
        
        var newShortcuts:[AppShortcut] = []
        
        let userObject = DataHandler.sharedInstance.user

        if(Helper.getUser() == nil) //|| userObject?.is_requested == 1)
        {
            UIApplication.shared.shortcutItems?.removeAll()
            return
        }
        
        if(userObject?.user_type != kBusyBee)
        {
            newShortcuts.append(AppShortcut(type: ShortcutIdentifier.isTypeVechil.type, title: kTopNavigationVechile, icon:"vechile_inpection", segue: ShortcutIdentifier.isTypeVechil.type))
            newShortcuts.append(AppShortcut(type: ShortcutIdentifier.isTypeHouse.type, title: kTopNavigationHouse, icon:"house_inpection", segue: ShortcutIdentifier.isTypeHouse.type))
            newShortcuts.append(AppShortcut(type: ShortcutIdentifier.isTypeEqupment.type, title: kTopNavigationEquipment, icon:"equpment_inpection", segue: ShortcutIdentifier.isTypeEqupment.type))
            newShortcuts.append(AppShortcut(type: ShortcutIdentifier.isTypeWebsite.type, title: kTopNavigationWebsite, icon:"website_inpection", segue: ShortcutIdentifier.isTypeWebsite.type))
        
        }
        
        newShortcuts.append(AppShortcut(type: ShortcutIdentifier.isTypePast.type, title: "Past Request", icon:"past_verification", segue: ShortcutIdentifier.isTypePast.type))
        
        UIApplication.shared.shortcutItems = newShortcuts
        shortcuts = newShortcuts
    }
    
    class func performShortcut(window:UIWindow, shortcut:UIApplicationShortcutItem) {
        
        sync()
        
//        let shortCutType = shortcut.type as String?
        
      /*  var vc = UIViewController()
        
        switch shortCutType {
            case ShortcutIdentifier.isTypeVechil.type:
                let vcs = UIStoryboard.homeStoryboard().instantiateViewController(withIdentifier: HomeMainControllers.VechileInqureyRequestVC.rawValue) as! VechileInqureyRequestViewController
                vcs.image = UIImage.init(named:"\(images[0])_detail")
                vcs.imageDetails = UIImage.init(named:"\(images[0])_detail-1")
                vcs.titlestr = kTopNavigationVechile
                vc = vcs
            break
        case ShortcutIdentifier.isTypeEqupment.type:
            let vcs = UIStoryboard.eqiupmentVerifictionStoryBoard().instantiateViewController(withIdentifier: HomeMainControllers.EquipmentVerificty.rawValue) as! EquipmentVerificationViewController
            vcs.image = UIImage.init(named:"Driver_detail")
            vcs.imageDetails = UIImage.init(named:"\(images[3])_detail-1")
            vcs.titlestr = kTopNavigationEquipment
            vc = vcs
            break
        case ShortcutIdentifier.isTypeHouse.type:
            let vcs = UIStoryboard.homeverifctionvBoard().instantiateViewController(withIdentifier: HomeMainControllers.HomeVerification.rawValue) as! HomeVerificationViewController
            vcs.image = UIImage.init(named:"\(images[2])_detail")
            vcs.imageDetails = UIImage.init(named:"\(images[2])_detail-1")
            vcs.titlestr = kTopNavigationHouse
            vc = vcs
            break
        case ShortcutIdentifier.isTypeWebsite.type:
            let vcs = UIStoryboard.websiteStoryborad().instantiateViewController(withIdentifier: HomeMainControllers.WebsiteVerifcation.rawValue) as! WebsiteVerificationViewController
            vcs.image = UIImage.init(named:"\(images[1])_detail")
            vcs.imageDetails = UIImage.init(named:"\(images[1])_detail-1")
            vcs.titlestr = kTopNavigationWebsite
            vc = vcs
            break
        case ShortcutIdentifier.isTypePast.type:
            let vcs = UIStoryboard.contentStoryboard().instantiateViewController(withIdentifier: ContentsControllers.PastVerificationVC.rawValue) as! PastVerificationVC
            vcs.isNotifictionObject = false
            vc = vcs
            break
        case .none: break
            
        case .some(_): break
            
        }
        
        if let rootNavigationViewController = window.rootViewController as? UINavigationController{
            rootNavigationViewController.pushViewController(vc, animated: true)
        }
          */
    }
}
