//
//  UIView+Extension.swift
//  TheMall
//
//  Created by APP on 1/9/18.
//  Copyright © 2018 APP. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func cornerRadiusandShadow(){
        DispatchQueue.main.async {
            self.layer.cornerRadius = 10
            let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10)
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 0 , height: 1);
            self.layer.shadowOpacity = 0.2
            self.layer.shadowPath = shadowPath.cgPath
        }
    }
    
    func topCorners(){
        DispatchQueue.main.async {
            let radius = 10.0
            let corners:UIRectCorner = [.topLeft, .topRight]
            let shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 0 , height: 1);
            self.layer.shadowOpacity = 0.2
            self.layer.shadowPath =  shadowPath.cgPath
        }
    }
    
    func bottomCorners(){
        DispatchQueue.main.async {
//            self.layer.cornerRadius = 10
//            let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 0)
//            self.layer.shadowColor = UIColor.black.cgColor
//            self.layer.shadowOffset = CGSize(width: 0 , height: 1);
//            self.layer.shadowOpacity = 0.2
//            self.layer.shadowPath = shadowPath.cgPath
//            self.roundCorners([.bottomLeft, .bottomRight], radius: 10)
//            self.roundCorners([.topLeft, .topRight], radius: 10)
            let radius = 10.0
            let corners:UIRectCorner = [.bottomLeft, .bottomRight]
            let shadowPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 0 , height: 1);
            self.layer.shadowOpacity = 0.2
            self.layer.shadowPath =  shadowPath.cgPath
        }
    }
    
    func parallelShadow(){
        DispatchQueue.main.async {
//            self.roundCorners([.bottomLeft, .bottomRight], radius: 10)
            let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 0)
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 0 , height: 1);
            self.layer.shadowOpacity = 0.2
            self.layer.shadowPath =  shadowPath.cgPath
            //            self.roundCorners([.topLeft, .topRight], radius: 10)
            //self.roundCorners([.bottomLeft, .bottomRight], radius: 2)
        }
    }
//    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        self.layer.mask = mask
//    }
    
    func animateView(view:UIView){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 10, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 10, y: view.center.y))
        
        view.layer.add(animation, forKey: "position")
    }
    
    func circle(){
        DispatchQueue.main.async {
            self.cornerRadius = self.frame.height/2
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
 
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
            let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10)
        layer.shadowPath = shadowPath.cgPath
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowOffset = CGSize(width: -1, height: 2)
        layer.shadowRadius = 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
 
            self.layer.masksToBounds = false
            self.layer.shadowColor =  color.cgColor
            self.layer.shadowOffset = offSet
            self.layer.shadowOpacity = opacity
            let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius)
            self.layer.shadowPath = shadowPath.cgPath
            layer.shouldRasterize = true
            layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        
    }
    
    func applyGradient(withColours colours: [UIColor], locations: [NSNumber]? = nil){ 
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(withColours colours: [UIColor], gradientOrientation orientation: GradientOrientation) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    
}

typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint {
        return points.startPoint
    }
    
    var endPoint : CGPoint {
        return points.endPoint
    }
    
    var points : GradientPoints {
        get {
            switch(self) {
            case .topRightBottomLeft:
                return (CGPoint(x: 0.0,y: 1.0), CGPoint(x: 1.0,y: 0.0))
            case .topLeftBottomRight:
                return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 1,y: 1))
            case .horizontal:
                return (CGPoint(x: 0.0,y: 0.5), CGPoint(x: 1.0,y: 0.5))
            case .vertical:
                return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 0.0,y: 1.0))
            }
        }
    }
}


@IBDesignable
class shadowForView: UIView {
    
    @IBInspectable var setBorder : Bool = true{
        didSet{
            
            DispatchQueue.main.async {
                
//                self.layer.borderColor = UIColor.green.cgColor
//                self.layer.borderWidth = 1
//                self.layer.cornerRadius = 10
//                let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 10)
//                self.layer.masksToBounds = true
//                self.layer.shadowColor = UIColor.black.cgColor
//                self.layer.shadowOffset = CGSize(width: 0 , height: 1);
//                self.layer.shadowOpacity = 0.5
//                self.layer.shadowPath = shadowPath.cgPath
            }
//            self.layer.borderColor = UIColor.white.cgColor
//            self.layer.borderWidth = 1
//            let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius)
//            //            self.layer.masksToBounds = true
//            self.layer.shadowColor = UIColor.black.cgColor
//            self.layer.shadowOffset = CGSize(width: 0 , height: 1);
//            self.layer.shadowOpacity = 0.2
//            self.layer.shadowPath = shadowPath.cgPath
            
        }
    }
}


@IBDesignable
class borderedImageView: UIImageView {
    
    
    @IBInspectable var setBorder : Bool = true{
        didSet{
            self.layer.borderColor = UIColor.gray.cgColor
            self.layer.borderWidth = 0.5
            let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius)
            //            self.layer.masksToBounds = true
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 0 , height: 1);
            self.layer.shadowOpacity = 0.2
            self.layer.shadowPath = shadowPath.cgPath
            
        }
    }
}


@IBDesignable
class CurvedUIImageView: UIImageView {

    private func pathCurvedForView(givenView: UIView, curvedPercent:CGFloat) ->UIBezierPath
    {
        let arrowPath = UIBezierPath()
        arrowPath.move(to: CGPoint(x:0, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.bounds.size.width, y:0))
        arrowPath.addLine(to: CGPoint(x:givenView.bounds.size.width, y:givenView.bounds.size.height))
        arrowPath.addQuadCurve(to: CGPoint(x:0, y:givenView.bounds.size.height), controlPoint: CGPoint(x:givenView.bounds.size.width/2, y:givenView.bounds.size.height-givenView.bounds.size.height*curvedPercent))
        arrowPath.addLine(to: CGPoint(x:0, y:0))
        arrowPath.close()
        
        return arrowPath
    }
    
    @IBInspectable var curvedPercent : CGFloat = 0{
        didSet{
            guard curvedPercent <= 1 && curvedPercent >= 0 else{
                return
            }
            
            // update the curvelayer path
            let path = UIBezierPath(ovalIn: self.getCurveFrame())
            let shapeLayer = CAShapeLayer(layer: self.layer)
            shapeLayer.path = path.cgPath
            self.layer.mask = shapeLayer
        }
    }
   
    


// method to retrive the frame for the curve layer (depending on the main view size)

    
    private func getCurveFrame() -> CGRect {
    let height = self.frame.height
    let width = self.frame.width
    
    return CGRect(x: -30, y: -width * 2.5 + width*2.5, width: height * 2, height: width * 3)
}
}

@IBDesignable
class UIButtonExtension: UIButton {
    
    
    // when you want to change highlighted color
    override var isHighlighted: Bool {
        didSet {
            if (isHighlighted) {
                layer.removeAnimation(forKey: "borderColor")
                layer.borderColor = borderColor?.withAlphaComponent(0.2).cgColor
            } else {
                layer.borderColor = borderColor?.cgColor
                let color = CABasicAnimation(keyPath: "borderColor")
                color.duration = 0.2
                color.fromValue = borderColor?.withAlphaComponent(0.2).cgColor
                color.toValue = borderColor?.cgColor
                layer.add(color, forKey: "borderColor")
            }
        }
    }
}

@IBDesignable
class UITextFieldCustomExtention: UITextField {
    
//    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5);
//    
//    override open func textRect(forBounds bounds: CGRect) -> CGRect {
//        return UIEdgeInsetsInsetRect(bounds, padding)
//    }
//    
//    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        return UIEdgeInsetsInsetRect(bounds, padding)
//    }
//    
//    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return UIEdgeInsetsInsetRect(bounds, padding)
//    }

    @IBInspectable var placeHolderColor: UIColor? {
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
        
        get {
            return self.placeHolderColor
        }
    }
}
