//
//  UIViewController+Extension.swift
//  AbrahimLegacy
//
//  Created by Nouman Gul Junejo on 11/11/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com All rights reserved.
//

import UIKit
import LGSideMenuController

extension UIViewController {
    
    func setBarLeftMenu() {
        let menuButton = UIBarButtonItem(image: UIImage(named: "icon_menu"), style: .plain, target: self, action: #selector(menuButtonTapped))
        menuButton.image = menuButton.image?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = menuButton
    }
    func setRightBarButton(imageName : String){
        var rightImage = UIImage(named: imageName)
        rightImage = rightImage?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: rightImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackButton))
    }
    
    func setBarBackButton(){

        var rightImage = UIImage(named: "icon_back")
        rightImage = rightImage?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: rightImage, style: UIBarButtonItem.Style.plain, target: self, action: #selector(onBackButton))
    }
    
    @objc func onBackButton() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func menuButtonTapped() {
        self.sideMenuController?.showLeftView()
    }
    
    @IBAction func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
