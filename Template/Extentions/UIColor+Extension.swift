//
//  UIColor+Extension.swift
//  TheMall
//
//  Created by Nouman Gul Junejo on 1/10/18.
//  Copyright © 2018 Noumanguljunejo@gmail.com All rights reserved.
//

import UIKit

extension UIColor {
    
    static func getColorFromRGB(red:CGFloat,green:CGFloat ,blue:CGFloat ) -> UIColor{
      return UIColor.init(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0)
    }
    static func appBlueHexColor() -> UIColor {
        //blue
        return hexStringToUIColor(hex: "#3B5998")
    }
    
    static func appLoaderColor() -> UIColor {
        //blue
        return hexStringToUIColor(hex: "#E5E7E9")
    }
    
    static func appBlueColor() -> UIColor{
        return UIColor.init(red: 13.0/255.0, green: 97.0/255.0, blue: 163.0/255.0, alpha: 1.0)
    }
    
    static func appGoldenColor()-> UIColor {
        return hexStringToUIColor(hex: "#ED5B2A")
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
