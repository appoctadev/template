//
//  NetworkViewModel.swift
//  Template
//
//  Created by MacBook on 1/18/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class NetworkViewModel
{
    var kApiPath = kEmptyString
    var kRequestType = webapiCallType.postRequst
    var parameter = Parameters()
    
    static let shared = NetworkViewModel()
    
    var imageData = NSMutableArray()
    
    init() {
       intilizeAnimation()
    }
   
    
    init(paremter: Parameters, urlPath: String, postType: webapiCallType)
    {
        self.parameter = paremter
        self.kApiPath = urlPath
        self.kRequestType = postType
    }
    
    func setParamter(paremter: Parameters, urlPath: String, postType: webapiCallType)
    {
        self.parameter = paremter
        self.kApiPath = urlPath
        self.kRequestType = postType
    }
    
    func setImageDataParamters(paremter: Parameters, urlPath: String, postType: webapiCallType, imageDataArray: NSMutableArray)
    {
        self.parameter = paremter
        self.kApiPath = urlPath
        self.kRequestType = postType
        self.imageData = imageDataArray
    }
}


extension NetworkViewModel {
    
    /*
     // MARK: - Network Call
     */
    
    func get(completion :@escaping (Any) -> ())
    {
        // Use a URL builder scheme here instead of just using the string URL
        switch self.kRequestType {
        case .postRequst:
            self.callPostAPI(urlPath: self.kApiPath, completion: completion)
            break
        case .none:
            break
        case .getRequst:
            self.callGetAPI(urlPath: self.kApiPath, completion: completion)
            break
        case .putRequst:
            self.callPut(urlPath: self.kApiPath, completion: completion)
            break
        case .deleteRequst:
            self.DeleteObject(urlPath: self.kApiPath, completion: completion)
            break
        case .uploadImages:
            self.uploadImageUsingMultiPart(urlPath: self.kApiPath, requestType: self.kRequestType, completion: completion)
            break
        case .uploadVideo:
            self.uploadImageUsingMultiPart(urlPath: self.kApiPath, requestType: self.kRequestType, completion: completion)
            break
        case .uplaodAudio:
            self.uploadImageUsingMultiPart(urlPath: self.kApiPath, requestType: self.kRequestType, completion: completion)
            break
        }
        
        // Instead of getArticles method webservice should be generic and return the requested
        // object/objects of requested type
        
       /* self.apiClient.callAPI(url: url) { (sample) in
            
            self.mubleArr = self.mubleArr.compactMap { article in
                NetworkViewModel(array: [article])
            }
            
            DispatchQueue.main.async {
                completion(self)
            }
        }*/
        
        
       /* self.webservice.getArticles(url: url) { articles in
            
            self.articles = articles.flatMap { article in
                NetworkViewModel(article: article)
            }
        }*/
    }
    
    
    /*
     // MARK: - Multipart Image
     */
    func uploadImageUsingMultiPart(urlPath: String, requestType: webapiCallType , completion: @escaping (Any) -> ())
    {
        ApiHelper.postImageMultipartRequest(self.parameter as [String : AnyObject], multiPartParams: self.imageData, kPath: urlPath, requestType: requestType) { (server_reponseObject, error) in
            
            if error != nil{
                let errorMessage = error?.domain
                showToast(view: (appDelet.window?.rootViewController?.view)!, message: errorMessage!)
                return
            }
            
            let resonseObject = self.parseObjectValues(responseObject: server_reponseObject as Any, urlPath: urlPath)
            completion(resonseObject as Any)
        }
    }
    /*
     // MARK: - Calling GET API
     */
    func callGetAPI(urlPath: String , completion: @escaping (Any) -> ())
    {
        ApiHelper.getRequest(self.parameter as [String : AnyObject], kPath: urlPath)
        {
            (server_reponseObject, error) in
            
            if error != nil{
                let errorMessage = error?.domain
                showToast(view: (appDelet.window?.rootViewController?.view)!, message: errorMessage!)
                return
            }
            
            let resonseObject = self.parseObjectValues(responseObject: server_reponseObject as Any, urlPath: urlPath)
            completion(resonseObject as Any)
        }
    }
    
    /*
     // MARK: - Calling Put API
     */
    func callPut(urlPath: String , completion: @escaping (Any) -> ())
    {
        ApiHelper.putRequest(self.parameter as [String : AnyObject], kPath: urlPath)
        {
            (server_reponseObject, error) in
            
            if error != nil{
                let errorMessage = error?.domain
                showToast(view: (appDelet.window?.rootViewController?.view)!, message: errorMessage!)
                return
            }
            
            let resonseObject = self.parseObjectValues(responseObject: server_reponseObject as Any, urlPath: urlPath)
            completion(resonseObject as Any)
        }
    }
    
    /*
     // MARK: - Delete API
     */
    func DeleteObject(urlPath: String , completion: @escaping (Any) -> ())
    {
        ApiHelper.deleteRequest(self.parameter as [String : AnyObject], kPath: urlPath)
        {
            (server_reponseObject, error) in
            
            if error != nil{
                let errorMessage = error?.domain
                showToast(view: (appDelet.window?.rootViewController?.view)!, message: errorMessage!)
                return
            }
            
            let resonseObject = self.parseObjectValues(responseObject: server_reponseObject as Any, urlPath: urlPath)
            completion(resonseObject as Any)
        }
    }
    
    
    /*
     // MARK: - Calling POST API
     */
    
    func callPostAPI(urlPath: String , completion: @escaping (Any) -> ())
    {
        ApiHelper.postRequest(self.parameter as [String : AnyObject], kPath: urlPath)
        {
            (server_reponseObject, error) in
            
            if error != nil{
                let errorMessage = error?.domain
                showToast(view: (appDelet.window?.rootViewController?.view)!, message: errorMessage!)
                return
            }
            
            let resonseObject = self.parseObjectValues(responseObject: server_reponseObject as Any, urlPath: urlPath)
            completion(resonseObject as Any)
        }
    }
    
    /*
     // MARK: - PARSE Object Mapper
     */
    func parseObjectValues(responseObject: Any, urlPath: String ) -> AnyObject?
    {
        var responseObjectValue : AnyObject?
        switch urlPath
        {
        case APiCallPath.kApiLogin.rawValue:
            responseObjectValue = Mapper<User>().map(JSON: responseObject as! [String : Any])
            break
        case kApiLogin:
            
            break
        default:
            break
        }
        
//        DispatchQueue.main.async {
//            completion(responseObjectValue as Any)
//        }
        
        return responseObjectValue 
    }
}
