//
//  AppDelegate.swift
//  Template
//
//  Created by Nouman Gul on 1/15/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import IQKeyboardManagerSwift
import GoogleMaps
import FirebaseMessaging

import LinkedinSwift

import UserNotifications
import Firebase

import CRNotifications
import GooglePlaces

import Firebase

import FacebookCore
import FacebookLogin
import LinkedinSwift

import FacebookCore
import FacebookLogin
import FBSDKLoginKit

import FBSDKShareKit
import TwitterKit

import GoogleSignIn
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    /*
     // MARK: - AppDelegate Functions
     */
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        let backImage = UIImage(named: "icon_back")?.withRenderingMode(.alwaysOriginal)
//        UINavigationBar.appearance().backIndicatorImage = backImage
//        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
//        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: -30.0), for: .default)
        
        inititePushAndMapsFunctions()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    /*
     // MARK: - Initize API Variables
     */
    func inititePushAndMapsFunctions()
    {
        // initize api key and google maps key
        let kInitiAllApi = InitizeKeys()
        
        if(kInitiAllApi == true)
        {
            TWTRTwitter.sharedInstance().start(withConsumerKey: twitter_consumer_key, consumerSecret: twitter_consumer_secret)
            
            GIDSignIn.sharedInstance().clientID = google_clientId
            
            // Intilizing the GMS Services for Google Maps.
            GMSPlacesClient.provideAPIKey(GoogleMapsAPIKey)
            
            // Intilizing the GMS Services for Google Maps.
            GMSServices.provideAPIKey(GoogleMapsAPIKey)
        }
        else
        {
            showToast(view: (appDelet.window?.rootViewController?.view)!, message: kAPiNotInitize)
        }
        
//        let langueage = L102Language.currentAppleLanguage()
        
        L102Language.setAppleLAnguageTo(lang: "en")
        
        IQKeyboardManager.shared.enable = true
        
        setUserDefaultValue()
        
        promptUserToRegisterPushNotifications()
    }
    
    func setUserDefaultValue()
    {
        let defaults = UserDefaults.standard
        if defaults.value(forKey: "SoundActive") == nil
        {
            defaults.set(true, forKey: "SoundActive")
            
            defaults.set("0", forKey: kUserNotifictionCoutner)
            
            setDefaultValue(keyValue: kdeviceToken, valueIs: kEmptyString)
            
            setDefaultValue(keyValue: kNotificationCounter, valueIs: "0")
            
            setDefaultValue(keyValue: kurname, valueIs: "")
            setDefaultValue(keyValue: kpaswrd, valueIs: "")
            
            appDeletForBadee.applicationIconBadgeNumber = 0
            
            
            var appGroupDefaults = UserDefaults.standard
            appGroupDefaults = UserDefaults(suiteName:"group.com.cmolds.BusyBee")!
            
            appGroupDefaults.setValue("false", forKey: "isUserLogin")
            appGroupDefaults.setValue("empty", forKey: "userType")
            
            appGroupDefaults.synchronize()
        }
    }

    /*
     // MARK: - Push notifiction popup
     */
    
    func promptUserToRegisterPushNotifications() {
        // Register for Push Notifications
        
        if let filePath = Bundle.main.path(forResource: kFirebasePushFilePlist, ofType: "plist")
        {
            _ = NSDictionary(contentsOfFile: filePath)
//            print(dataDictionary)
            setupPushNotification()
        }
        else
        {
            showToast(view: (self.window?.rootViewController?.view)!, message: kPushNotifictionNotSetup)
        }
        
    }
    
    func setupPushNotification()
    {
        let application: UIApplication = UIApplication.shared

        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        //        Messaging.messaging().shouldEstablishDirectChannel = true
        // [END set_messaging_delegate]
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        printObject(objectPrint: deviceTokenString)
        
        // Persist it in your backend in case it's new
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        printObject(objectPrint:"APNs registration failed: \(error)")
    }
    
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject])
    {
        printObject(objectPrint:"Recived: \(userInfo)")
        //showAlert(controller: self.window, messageStr: "asdfasdf", titleStr: "")
        
        
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        printObject(objectPrint: userInfo)
        
        /* if(condition)
         {
         completionHandler(.noData);
         }
         else //fix
         {
         completionHandler(.newData)
         }*/
        
        //        completionHandler(.newData)
        //
        
        if(application.applicationState == .inactive)
        {
            if let aps = userInfo["aps"] as? NSDictionary {
                //self.infoDic = aps
                
                self.pushNotifiction(info:aps)
            }
            //              self.pushNotifiction(info:aps)
        }
        else
        {
            if let aps = userInfo["aps"] as? NSDictionary {
                self.pushNotifiction(info:aps)
            }
        }
        
        if let str = userInfo["custom"] as? String {
            
            printObject(objectPrint:str)
            //let dict = convertToDictionary(text: str)
            
            do{
                if let json = str.data(using: String.Encoding.utf8){
                    if let jsonData = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String:AnyObject]{
                        
                        //let id = jsonData["id"] as! String
                        let dict = jsonData
                        
                        let noti_fictionType = dict["type"] as? String
                        if(noti_fictionType != nil)
                        {
                            switch noti_fictionType
                            {
                            case kPushNotificaitonTypeAddReqeust:
                                if let inspection = dict["inspection"] as? NSDictionary
                                {
                                    printObject(objectPrint: inspection)
                                    //self.setDictionaryObjects(inspection: inspection)
                                }
                                break
                                
                            case kPushNotificaitonTypeAcceptReqeust:
                                break
                                
                            case kPushNotificaitonTypeReqeustVerified:
                                break
                                
                            default:
                                break
                            }
                        }
                    }
                }
            }catch {
                printObject(objectPrint:error.localizedDescription)
                
            }
            
            
        }
    }
    
    func pushNotifiction(info: NSDictionary)
    {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        let alrtObject = info["alert"] as! NSDictionary
        let tileofMSg = alrtObject.object(forKey: "title") as! String
        let msgOfBody = alrtObject.object(forKey: "body") as! String
        
        var counterOfNoticin = 1
        if(getValueForKey(keyValue: kNotificationCounter) == kEmptyString)
        {
            counterOfNoticin = 1
        }
        else
        {
            let countr = Int(getValueForKey(keyValue: kNotificationCounter))
            counterOfNoticin = 1
            
            counterOfNoticin = countr!
            counterOfNoticin+=1
            
        }
        
        CRNotifications.showNotification(type: CRNotifications.success, title: tileofMSg, message: msgOfBody, dismissDelay: 3)
        
        //        appDeletForBadee.applicationIconBadgeNumber = counterOfNoticin
        //        setDefaultValue(keyValue: kNotificationCounter, valueIs: String(counterOfNoticin))
    }
    
    
    /*
    // MARK: - Webservice Function
    */
    func hitWebserviceUpdateToken(fcmToken: String)
    {
        var uUID = UIDevice.current.identifierForVendor!.uuidString
        
        if(getValueForKey(keyValue: kSaveUDID) == kEmptyString)
        {
            saveUsrDefault(keyValue: uUID, valueIs: kSaveUDID)
        }
        else
        {
            uUID = getValueForKey(keyValue: kSaveUDID)
        }
        
        APIClient.RegisterDeviceTokenRequest(udid: uUID, token: fcmToken) { (object, error, message) in
            
        }
    }

    /*
     // Social Netwoking calling
    */
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        //        var options: [String: AnyObject] = [UIApplicationOpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject,
        //                                            UIApplicationOpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
        return GIDSignIn.sharedInstance().handle(url as URL,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    
    // MARK: - Facebook APP URL
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        _ = SDKSettings.appId
        
        let directedByFB = SDKApplicationDelegate.shared.application(app, open: url, options: options)
        let directedByTWTR =  TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        let directedByGGL =  GIDSignIn.sharedInstance().handle(url as URL,
                                                               sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        
        var linkedin = false
        // Linkedin sdk handle redirect
        if LinkedinSwiftHelper.shouldHandle(url as URL)
        {
            let linkedin1 = LinkedinSwiftHelper.application(app,
                                                           open: url,
                                                           sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                           annotation: options[UIApplication.OpenURLOptionsKey.annotation])
            
            linkedin = linkedin1;
        }
        
        return directedByFB || directedByTWTR || directedByGGL || linkedin
        //com.venmo.touch.v2://x-callback-url/vzero/auth
        //        return false
    }
}



@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        
        completionHandler([.alert,.sound])
       
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        _ = response.notification.request.content.userInfo
        // Print message ID.
        
        if(response.notification.request.identifier == "cocoacasts_local_notification" )
        {
            
            
        }
        
        completionHandler()
        
    }
}



// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        printObject(objectPrint:"Firebase registration token: \(fcmToken)")
        
        setDefaultValue(keyValue: kdeviceToken, valueIs: fcmToken)
        
        if(Helper.getUser() != nil)
        {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.hitWebserviceUpdateToken(fcmToken: fcmToken)
        }
        
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        printObject(objectPrint:"Received data message: \(remoteMessage.appData)")
        
        //        let mainObject = remoteMessage.appData["notification"] as? NSDictionary
        //
        //        if(mainObject != nil)
        //        {
        //          //  let mainObject = remoteMessage.appData["notification"] as! NSDictionary
        //            let tileofMSg = mainObject?["title"] as! String
        //            let msgOfBody = mainObject?["body"] as! String
        //
        //            CRNotifications.showNotification(type: .success, title: tileofMSg, message: msgOfBody, dismissDelay: 3)
        //
        //        }
    }
    // [END ios_10_data_message]
}
