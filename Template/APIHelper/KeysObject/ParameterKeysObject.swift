//
//  ParameterKeysObject.swift
//  Template
//
//  Created by MacBook on 1/22/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit
import Alamofire

class ParameterKeysObject: NSObject {
    
    static var paramert = Parameters()
    
    override init() {
        
    }
    
    static  func setupParamterObject(emailObject: String, pasword: String, udid: String, apiCallType: String) -> Parameters
    {
        
        
        switch apiCallType
        {
        case APiCallPath.kApiLogin.rawValue:
            self.paramert["email"] = emailObject
            self.paramert["password"] = pasword
            self.paramert["udid"] = udid
            break
        default:
            break
        }
        
        return self.paramert
    }
    
    static func createParameterOfImage(idValue: String, comment: String, imagesObject: NSMutableArray, handler:@escaping (_ mutableArray:NSMutableArray?, _ paramter: Parameters?)-> Void)
    {
        self.paramert["id"] = idValue
        self.paramert["comment"] = comment
        
        let imagesData = NSMutableArray()

        for i in 0 ..< imagesObject.count
        {
            let imagesObjectDic = imagesObject[i] as! NSMutableDictionary
            
            let imagesKeyDic = NSMutableDictionary()
            let keyValue = imagesObjectDic.object(forKey: "keyValue") as! String
            
            let keyIndex = "image["+String(i)+"]"
            imagesKeyDic.setObject(keyValue, forKey: keyIndex as NSCopying)
            
            let imageValue = imagesObjectDic.object(forKey: "image") as! UIImage
            let imageData = imageValue.jpegData(compressionQuality: 0.75)
            imagesKeyDic.setObject(imageData as Any, forKey: keyIndex as NSCopying)
            
            imagesData.add(imagesKeyDic)
        }
        
        handler(imagesData, self.paramert)
    }
}
