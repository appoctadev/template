//
//  APIConstant.swift
//  Template
//
//  Created by MacBook on 1/18/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire

//MARK:- All SocialNetworking Variables
var twitter_consumer_key: String = ""
var twitter_consumer_secret: String = ""

var baseUrl: String = ""

var GoogleMapsAPIKey: String = ""
var google_clientId: String = ""

var linkedIn_clientId: String = ""
var linkedIn_clientSecret: String = ""
var linkedIn_clientState: String = ""
var linkedIn_clientRedirectURL: String = ""
var linkedIn_apiURL: String = ""

let webservice = APIClient.shared

//MARK:- API Functions
let kRegister = "register"
let kApiLogin = "login"
let kForgotPass = "forget-password"
let kCategoryList = "category/list"
let kUpdateProfile = "update"
let kInpectionFeeURLJSON = "inspection/fee"
let kInitiateVechileFormJSON = "inspection/request"
let kUpdateDeviceTokenJSON = "device-token/set"
let kRemoveDeviceToken = "device-token/remove"
let kUpdateUserStatusJSON = "user/status/update"
let kGetAllInpectionHistoryJSON = "inspection/history"
let kGetAllInpectionNotifictionHistoryJSON = "inspection/notifications"
let kCehckIfPreviuosInpectionJSON = "view"
let kPageContentJSON = "pages"
let kAcceptRequestJSON = "inspection/accept"
let kInpectionViewJSON = "inspection/view"
let kUplaodCommentJSON = "inspection/report/upload"

enum APiCallPath: String {
    case kApiLogin = "login",
    kAPiSignup = "signup",
    kUploadMultipleImages = "inspection/report/upload"
}

enum webapiCallType: String
{
    case postRequst
    case getRequst
    case putRequst
    case deleteRequst
    case uploadImages
    case uploadVideo
    case uplaodAudio
    case none
}

//MARK:- Initize API Functions
func InitizeKeys() -> Bool
{
    if let url = Bundle.main.url(forResource:"SocialNetworkKeys", withExtension: "plist") {
        do {
            let data = try Data(contentsOf:url)
            let dictPlistObject = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [String:Any]
            
            // MARK: - Twitter Key
            twitter_consumer_key = dictPlistObject["twitter_consumer_key"] as! String
            twitter_consumer_secret = dictPlistObject["twitter_consumer_secret"] as! String
            
            // MARK: - Base URL
            baseUrl = dictPlistObject["baseUrl"] as! String
            
            // MARK: - Google Maps & Google login AppId
            GoogleMapsAPIKey = dictPlistObject["GoogleMapsServicesAPIKey"] as! String
            google_clientId = dictPlistObject["google_client_id"] as! String
            
            // MARK: - LinkedIn keys
            linkedIn_clientId = dictPlistObject["linkedin_client_id"] as! String
            linkedIn_clientSecret = dictPlistObject["linkedin_clientSecret"] as! String
            linkedIn_clientState = dictPlistObject["linkedin_state"] as! String
            linkedIn_clientRedirectURL = dictPlistObject["linkedin_redirect_url"] as! String
            linkedIn_apiURL = dictPlistObject["linkedin_api_url"] as! String
            
            return true
            
        } catch {
            printObject(objectPrint:error)
            return false
        }
    }
    
    return false
}
