//
//  ApiHelper.swift
//  ittendance
//
//  Created by Nouman Gul Junejo on 5/3/17.
//  Copyright © 2017 Nouman Gul Junejo. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class ApiHelper {

}

enum ResponseKey: String {
    case error = "error",success = "response", data = "data"
}

extension ApiHelper {
    
    static let shared = ApiHelper()
    
    // MARK: POST REQUEST
    @discardableResult static func postRequest(_ params: [String : AnyObject]?, kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) -> DataRequest {
        
        APIClient.checkAndShowAlert()
        
        let url = baseUrl + kPath
        printObject(objectPrint:"Web URL == \(url)")
        printObject(objectPrint:"Param == \(String(describing: params))")
        
        let header = DataHandler.sharedInstance.getHeader(contentType: true)
        let request = Alamofire.request(url, method: .post,
                                        parameters: params,
                                        encoding: JSONEncoding.default,
                                        headers: header
                                        ).responseJSON { (response: DataResponse<Any>) in
                                            isShowIndicator(showIndica: false)
                                          
                                            printObject(objectPrint:"response APIPATH \(kPath): ===error=== \(String(describing: response.result.error))")
                                            printObject(objectPrint:"response APIPATH \(kPath): ===value=== \(String(describing: response.result.value))")
                                            
                                            
                                            guard response.result.error == nil else { // error for eg : network unavailable
                                                
                                                let err = response.result.error as NSError?
                                                if err?.code != NSURLErrorCancelled {
                                                    
                                                    let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                                                    let error = NSError(domain: unableToConnect,
                                                                        code: (err?.code)!,
                                                                        userInfo: [NSLocalizedDescriptionKey : message ?? unableToConnect])
                                                    completion(nil, error)
                                                    
                                                }
                                                return
                                            }
                                            
                                            let reponseObject = response.result.value as? [String:AnyObject?]
                                            
                                            guard reponseObject != nil else { // reponse is nil
                                                
                                                let error = NSError(domain: unableToConnect,
                                                                    code: 404,
                                                                    userInfo: [NSLocalizedDescriptionKey :unableToConnect])
                                                completion(nil, error)
                                                
                                                return;
                                            }
                                            
                                            if (reponseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                                                
                                                printObject(objectPrint:"Response object: \(String(describing: reponseObject))")
                                                
                                                
                                                completion(reponseObject![ResponseKey.success.rawValue]??[ResponseKey.data.rawValue] as AnyObject?, nil)
                                                
                                                
                                            } else if (reponseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                                                
                                                
                                                let responseData = Mapper<GeneralResponse>().map(JSON: reponseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                                               
                                                var message = kEmptyString
                                                if(responseData?.messages != nil)
                                                {
                                                    let messagessObj = responseData!.messages

                                                    var variableString = ""
                                                    for i in 0 ..< messagessObj!.count
                                                    {
                                                        let objectValue = messagessObj![i]
                                                        variableString += objectValue
                                                    }
                                                    
                                                    message = variableString
                                                }
                                                else
                                                {
                                                    message = kAPIErrorMessage
                                                }
                                                
                                                    printObject(objectPrint:"responseData postRequest ==\(String(describing: responseData))")
                                                    let error = NSError(domain: message,
                                                                        code: (responseData?.code)!,
                                                                        userInfo: [NSLocalizedDescriptionKey : message])
                                                    
                                                    completion(nil, error)
                                                
                                            }
        }
        
        return request
    }
    
    static func postImageMultipartRequest(_ params: [String : AnyObject]?,multiPartParams: NSMutableArray?, kPath: String,requestType : webapiCallType , completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void)
    {
        let url = baseUrl + kPath
        
        APIClient.checkAndShowAlert()
        
       // printObject(objectPrint:"API URL:\n \(String(describing: url)) ===params===\n\(String(describing: params)) ===multipart params===\n\(String(describing: multiPartParams)) ===headers===\n\(String(describing: DataHandler.sharedInstance.getHeader(contentType: false)))")
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            let countOfImag = (multiPartParams?.count ?? 0)
            for i in 0 ..< countOfImag
            {
                let imagesObject = multiPartParams![i] as! NSMutableDictionary

                if(imagesObject.allKeys.count > 0)
                {
                    let keyValue = imagesObject.allKeys[0] as! String //imagesObject.object(forKey: "imageKey") as! String
                    let imageData = imagesObject.object(forKey: keyValue)  as! Data
                    
                    if(requestType == .uplaodAudio)
                    {
                        multipartFormData.append(imageData, withName: keyValue, fileName: "file.m4a", mimeType: "audio/mp4")
                    }
                    else if(requestType == .uploadVideo)
                    {
                        multipartFormData.append(imageData, withName: keyValue, fileName: "file.mp4", mimeType: "video/mp4")
                    }
                    else // images
                    {
                        multipartFormData.append(imageData, withName: keyValue, fileName: "file.png", mimeType: "image/png")
                        multipartFormData.append("png".data(using: .utf8)!, withName: "type")
                    }
                }
            }
            
           /* let tmpImage = multiPartParams?["data"] as! UIImage
            if let imageData = tmpImage.jpegData(compressionQuality: 0.75)
            {
                multipartFormData.append(imageData, withName: multiPartParams?["parameter"] as! String, fileName: "file.png", mimeType: "image/png")
                multipartFormData.append("png".data(using: .utf8)!, withName: "type")
            }*/
            
           /* if let imageData = UIImageJPEGRepresentation(multiPartParams?["image"] as! UIImage , 1.0) {
                printObject(objectPrint:imageData.count/1024)
                multipartFormData.append(imageData, withName: multiPartParams?["parameter"] as! String, fileName: "image.jpg", mimeType:  "image/jpeg")
            }
            
            if let imageData = UIImageJPEGRepresentation(multiPartParams?["insuranceImage"] as! UIImage , 1.0) {
                printObject(objectPrint:imageData.count/1024)
                multipartFormData.append(imageData, withName: multiPartParams?["parameter"] as! String, fileName: "image.jpg", mimeType:  "image/jpeg")
            }*/
            
            if params != nil {
                
                for (key, value) in params! {
                    
                    let stringValue = "\(value)"
                    multipartFormData.append(((stringValue  as AnyObject).data(using: String.Encoding.utf8.rawValue))!, withName: key )
                    
                }}}, to: url, method: .post, headers: DataHandler.sharedInstance.getHeader(contentType: false),
                     encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                            
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                isShowIndicator(showIndica: false)
                                guard response.result.error == nil else { // error for eg : network unavailable
                                    
                                    let err = response.result.error as NSError?
                                    if err?.code != NSURLErrorCancelled {
                                        
                                        
                                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                                        let error = NSError(domain: "",
                                                            code: (err?.code)!,
                                                            userInfo: [NSLocalizedDescriptionKey : message ?? ""])
                                        completion(nil, error)
                                        
                                    }
                                    printObject(objectPrint: ("%@",err?.localizedDescription)  )
                                    
                                    return;
                                }
                                
                                let reponseObject = response.result.value as? [String:AnyObject?]
                                
                                guard reponseObject != nil else { // reponse is nil
                                    
                                    
                                    let error = NSError(domain: unableToConnect,
                                                        code: 404,
                                                        userInfo: [NSLocalizedDescriptionKey :unableToConnect])
                                    completion(nil, error)
                                    printObject(objectPrint: ("%@",error.localizedDescription ))
                                    
                                    return;
                                }
                                
                                
                                if (reponseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                                    
                                    printObject(objectPrint:"Response object: \(String(describing: reponseObject))")
                                    
                                    completion(reponseObject![ResponseKey.success.rawValue]??[ResponseKey.data.rawValue]! as AnyObject?, nil)
                                    
                                } else if (reponseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                                    
                                    let responseData = Mapper<GeneralResponse>().map(JSON: reponseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                                    
                                    var message = kEmptyString
                                    if(responseData?.messages != nil)
                                    {
                                        let messagessObj = responseData!.messages
                                        
                                        var variableString = ""
                                        for i in 0 ..< messagessObj!.count
                                        {
                                            let objectValue = messagessObj![i]
                                            variableString += objectValue
                                        }
                                        
                                        message = variableString
                                    }
                                    else
                                    {
                                        message = kAPIErrorMessage
                                    }
                                    
                                    let error = NSError(domain: message,
                                                        code: (responseData?.code)!,
                                                        userInfo: [NSLocalizedDescriptionKey : message])
                                    
                                    completion(reponseObject as AnyObject?, error)
                                    
                                }
                            }
                        case .failure(let encodingError):
                            completion(nil, encodingError as NSError?)
                            
                            printObject(objectPrint:"error:\(encodingError)")
                        }
        })
        
    }
    
    
    // MARK: POST MULTIPART IMAGE
    
    static func postMultipartRequest(_ params: [String : AnyObject]?,multiPartParams:[String:AnyObject]?, kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let url = baseUrl + kPath
        APIClient.checkAndShowAlert()
        
        printObject(objectPrint:"API URL:\n \(String(describing: url)) ===params===\n\(String(describing: params)) ===multipart params===\n\(String(describing: multiPartParams)) ===headers===\n\(String(describing: DataHandler.sharedInstance.getHeader(contentType: false)))")
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            if multiPartParams != nil {
                
                let tmpImage = multiPartParams?["data"] as! UIImage
                if let imageData = tmpImage.jpegData(compressionQuality: 0.75)
                {
                    multipartFormData.append(imageData, withName: multiPartParams?["parameter"] as! String, fileName: "file.png", mimeType: "image/png")
                    multipartFormData.append("png".data(using: .utf8)!, withName: "type")
                }
                
               /* if let imageData = UIImageJPEGRepresentation(multiPartParams?["data"] as! UIImage , 0.5) {
                    printObject(objectPrint:imageData.count/1024)
                    //multipartFormData.append(imageData, withName: multiPartParams?["parameter"] as! String, fileName: "image.jpg", mimeType:  "image/jpeg")
                    
                    multipartFormData.append(imageData, withName: "license", fileName: "image.jpg", mimeType:  "image/jpeg")
                }
                
                if multiPartParams?["insuranceImage"]  != nil {
                    if let imageData = UIImageJPEGRepresentation(multiPartParams?["insuranceImage"] as! UIImage , 1.0) {
                        printObject(objectPrint:imageData.count/1024)
//                        multipartFormData.append(imageData, withName: multiPartParams?["parameter"] as! String, fileName: "image.jpg", mimeType:  "image/jpeg")
                        multipartFormData.append(imageData, withName: "insurance", fileName: "image.jpg", mimeType:  "image/jpeg")
                    }
                }*/
                
            }
            
            
            
            if params != nil {
                
                for (key, value) in params! {
                    
                    let stringValue = "\(value)"
                    multipartFormData.append(((stringValue  as AnyObject).data(using: String.Encoding.utf8.rawValue))!, withName: key)
                    
                }}}, to: url, method: .post, headers: DataHandler.sharedInstance.getHeader(contentType: false),
                     encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                            
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                isShowIndicator(showIndica: false)
                                
                                guard response.result.error == nil else { // error for eg : network unavailable
                                    
                                    let err = response.result.error as NSError?
                                    if err?.code != NSURLErrorCancelled {
                                        
                                        
                                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                                        let error = NSError(domain: unableToConnect,
                                                            code: (err?.code)!,
                                                            userInfo: [NSLocalizedDescriptionKey : message ?? unableToConnect])
                                        completion(nil, error)
                                        
                                    }
                                    printObject(objectPrint: ("%@",err?.localizedDescription)  )
                                    
                                    return;
                                }
                                
                                let reponseObject = response.result.value as? [String:AnyObject?]
                                
                                guard reponseObject != nil else { // reponse is nil
                                    
                                    
                                    let error = NSError(domain: unableToConnect,
                                                        code: 404,
                                                        userInfo: [NSLocalizedDescriptionKey :unableToConnect])
                                    completion(nil, error)
                                    printObject(objectPrint:("%@",error.localizedDescription ))
                                    
                                    return;
                                }
                                
                                
                                if (reponseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                                    
                                    printObject(objectPrint:"Response object: \(String(describing: reponseObject))")
                                    
                                    completion(reponseObject![ResponseKey.success.rawValue]??[ResponseKey.data.rawValue]! as AnyObject?, nil)
                                    
                                } else if (reponseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                                    
                                    let responseData = Mapper<GeneralResponse>().map(JSON: reponseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                                    
                                    var message = kEmptyString
                                    if(responseData?.messages != nil)
                                    {
                                        let messagessObj = responseData!.messages
                                        
                                        var variableString = ""
                                        for i in 0 ..< messagessObj!.count
                                        {
                                            let objectValue = messagessObj![i]
                                            variableString += objectValue
                                        }
                                        
                                        message = variableString
                                    }
                                    else
                                    {
                                        message = kAPIErrorMessage
                                    }
                                    
                                    
                                    let error = NSError(domain: message,
                                                        code: (responseData?.code)!,
                                                        userInfo: [NSLocalizedDescriptionKey : message])
                                    
                                    completion(reponseObject as AnyObject?, error)
                                    
                                }
                            }
                        case .failure(let encodingError):
                            completion(nil, encodingError as NSError?)
                            
                            printObject(objectPrint:"error:\(encodingError)")
                        }
        })
        
    }
    
    static func postMultipartRequestAudioFile(_ params: [String : AnyObject]?,multiPartParams:[String:AnyObject]?, kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let url = baseUrl + kPath
        
        APIClient.checkAndShowAlert()
        printObject(objectPrint:"API URL:\n \(String(describing: url)) ===params===\n\(String(describing: params)) ===multipart params===\n\(String(describing: multiPartParams)) ===headers===\n\(String(describing: DataHandler.sharedInstance.getHeader(contentType: false)))")
        
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            multipartFormData.append(multiPartParams?["data"] as! Data, withName: multiPartParams?["parameter"] as! String, fileName: "audio.3gp", mimeType:  "video/3gpp")
            
            if params != nil {
                
                for (key, value) in params! {
                    
                    let stringValue = "\(value)"
                    multipartFormData.append(((stringValue  as AnyObject).data(using: String.Encoding.utf8.rawValue))!, withName: key)
                    
                }}}, to: url, method: .post, headers: DataHandler.sharedInstance.getHeader(contentType: false),
                     encodingCompletion: { encodingResult in
                        
                        switch encodingResult {
                            
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                isShowIndicator(showIndica: false)
                                
                                guard response.result.error == nil else { // error for eg : network unavailable
                                    
                                    let err = response.result.error as NSError?
                                    if err?.code != NSURLErrorCancelled {
                                        
                                        
                                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                                        let error = NSError(domain: unableToConnect,
                                                            code: (err?.code)!,
                                                            userInfo: [NSLocalizedDescriptionKey : message ?? unableToConnect])
                                        completion(nil, error)
                                        
                                    }
                                    printObject(objectPrint: ("%@",err?.localizedDescription)  )
                                    
                                    return;
                                }
                                
                                let reponseObject = response.result.value as? [String:AnyObject?]
                                
                                guard reponseObject != nil else { // reponse is nil
                                    
                                    
                                    let error = NSError(domain: unableToConnect,
                                                        code: 404,
                                                        userInfo: [NSLocalizedDescriptionKey :unableToConnect])
                                    completion(nil, error)
                                    printObject(objectPrint:("%@",error.localizedDescription ))
                                    
                                    return;
                                }
                                
                                
                                if (reponseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                                    
                                    printObject(objectPrint:"Response object: \(String(describing: reponseObject))")
                                    
                                    completion(reponseObject![ResponseKey.success.rawValue]??[ResponseKey.data.rawValue]! as AnyObject?, nil)
                                    
                                } else if (reponseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                                    
                                    let responseData = Mapper<GeneralResponse>().map(JSON: reponseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                                    
                                    var message = kEmptyString
                                    if(responseData?.messages != nil)
                                    {
                                        let messagessObj = responseData!.messages
                                        
                                        var variableString = ""
                                        for i in 0 ..< messagessObj!.count
                                        {
                                            let objectValue = messagessObj![i]
                                            variableString += objectValue
                                        }
                                        
                                        message = variableString
                                    }
                                    else
                                    {
                                        message = kAPIErrorMessage
                                    }
                                    
                                    let error = NSError(domain: message,
                                                        code: (responseData?.code)!,
                                                        userInfo: [NSLocalizedDescriptionKey : message])
                                    
                                    completion(reponseObject as AnyObject?, error)
                                    
                                }
                            }
                        case .failure(let encodingError):
                            completion(nil, encodingError as NSError?)
                            
                            printObject(objectPrint:"error:\(encodingError)")
                        }
        })
        
    }
    
    // MARK: PUT REQUEST
    
    
    static func putRequest(_ params: [String : AnyObject], kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let url = baseUrl + kPath
        
        APIClient.checkAndShowAlert()
//        let forgotPass = "https://dev-cmolds.com/appunitwork/lms/2ulearn/public/api/forgot-password"
//        url = kPath == Users.forgotPassword.rawValue ? forgotPass:url
        let header = ["Authorization": "p$M*2B@XI4rCz8jLN;Fn(m-5W5"] as [String:String]
        printObject(objectPrint:"put request api url: \(String(describing: url)) ===params===\(String(describing: params)) ===headers===: \(String(describing: DataHandler.sharedInstance.getHeader(contentType: true)))")
        
        Alamofire.request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response: DataResponse<Any>) in
            //Error handling
            
            isShowIndicator(showIndica: false)
            
            guard response.result.error == nil else {
                
                let err = response.result.error as NSError?
                if err?.code != NSURLErrorCancelled {
//                    if err?.code == errorCode.AuthorizationFailed.rawValue {
////                        Helper.popToLogin()
//                    }else{
                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                        let error = NSError(domain: unableToConnect,
                                            code: (err?.code)!,
                                            userInfo: [NSLocalizedDescriptionKey : message!])
                        completion(nil, error)
//                    }
                }
                return;
            }
            
            let responseObject = response.result.value as? [String : AnyObject]
            // if response contains no data
            guard responseObject != nil else {
                let error = NSError(domain: unableToConnect, code: 404, userInfo: [NSLocalizedDescriptionKey : unableToConnect])
                completion(nil, error)
                return;
            }
            
            if (responseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                
                printObject(objectPrint:"response object: \(String(describing: responseObject))")
                
                completion(responseObject![ResponseKey.success.rawValue]! as AnyObject?, nil)
                
            } else if (responseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                
                let responseData = Mapper<GeneralResponse>().map(JSON: responseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                
//                if responseData?.code == errorCode.AuthorizationFailed.rawValue {
//
//                } else{
                
                var message = kEmptyString
                if(responseData?.messages != nil)
                {
                    let messagessObj = responseData!.messages
                    
                    var variableString = ""
                    for i in 0 ..< messagessObj!.count
                    {
                        let objectValue = messagessObj![i]
                        variableString += objectValue
                    }
                    
                    message = variableString
                }
                else
                {
                    message = kAPIErrorMessage
                }
                    
                    let error = NSError(domain: message,
                                        code: (responseData?.code)!,
                                        userInfo: [NSLocalizedDescriptionKey : message])
                    
                    completion(nil, error)
//                }
                
            }
            
            // code commented because for dynamic use of this method
            /*let requestResult = Mapper<GeneralResponse>().map(JSON: responseObject![ResponseKey.success.rawValue] as! [String : Any])
             if let requestResult = requestResult {
             completion(requestResult, nil)
             } else {
             let error = NSError(domain: "Failed", code: 0 , userInfo: [NSLocalizedDescriptionKey : "Unknown error"])
             completion(nil, error)
             }*/
        }
        
    }
    // MARK: GET REQUEST
    
    @discardableResult static func getRequest(_ params: [String : AnyObject]?, kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) -> DataRequest {
        var url : String
        
        APIClient.checkAndShowAlert()
        url = baseUrl + kPath
        printObject(objectPrint:"webservice path: \(kPath): \(url)")
        printObject(objectPrint:params ?? "")
        
        let request = Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: DataHandler.sharedInstance.getHeader(contentType: true)).responseJSON { (response: DataResponse<Any>) in
            
            isShowIndicator(showIndica: false)
            guard response.result.error == nil else {
                
                let err = response.result.error as NSError?
                if err?.code != NSURLErrorCancelled {
                    
//                    if err?.code == errorCode.AuthorizationFailed.rawValue {
//                        Helper.popToLogin()
//                    }else{
                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                        let error = NSError(domain: unableToConnect,
                                            code: (err?.code)!,
                                            userInfo: [NSLocalizedDescriptionKey : message ?? ""])
                        completion(nil, error)
                   // }
                }
                return;
            }
            
            let responseObject = response.result.value as? [String:AnyObject?]
            guard responseObject != nil else {
                let error = NSError(domain: "No Data", code: 404, userInfo: [NSLocalizedDescriptionKey : unableToConnect])
                completion(nil, error)
                return;
            }
            if (responseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                
                completion(responseObject![ResponseKey.success.rawValue] as AnyObject?, nil)
                
            } else if (responseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                
                let responseData = Mapper<GeneralResponse>().map(JSON: responseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                
                //                if responseData?.code == errorCode.AuthorizationFailed.rawValue {
                //                    Helper.popToLogin()
                //                } else{
                
                var message = kEmptyString
                if(responseData?.messages != nil)
                {
                    let messagessObj = responseData!.messages
                    
                    var variableString = ""
                    for i in 0 ..< messagessObj!.count
                    {
                        let objectValue = messagessObj![i]
                        variableString += objectValue
                    }
                    
                    message = variableString
                }
                else
                {
                    message = kAPIErrorMessage
                }
                
                let error =  NSError(domain: message,
                                     code: (responseData?.code)!,
                                     userInfo: [NSLocalizedDescriptionKey : message])
                
                completion(nil, error)
                // }
            }else{
                completion(nil, nil)
            }
        }
        
        return request
    }
    
    // MARK: DELETE REQUEST
    
    static func deleteRequest(_ params: [String : AnyObject], kPath: String, completion: @escaping (_ data: AnyObject?, _ error: NSError?) -> Void) {
        let url = baseUrl + kPath
        printObject(objectPrint:"webservice path: \(kPath): \(url)")
        printObject(objectPrint:params)
        
        APIClient.checkAndShowAlert()
        
        Alamofire.request(url, method: .delete, parameters: params, encoding: URLEncoding.queryString, headers: DataHandler.sharedInstance.getHeader(contentType: true)).responseJSON { (response: DataResponse<Any>) in
            
            isShowIndicator(showIndica: false)
            guard response.result.error == nil else {
                
                let err = response.result.error as NSError?
                if err?.code != NSURLErrorCancelled {
//                    if err?.code == errorCode.AuthorizationFailed.rawValue {
//                        Helper.popToLogin()
//                    }else{
                        let message =  (err?.code == NSURLErrorNotConnectedToInternet ) ? err?.localizedDescription : unableToConnect
                        let error = NSError(domain: unableToConnect,
                                            code: (err?.code)!,
                                            userInfo: [NSLocalizedDescriptionKey : message ?? ""])
                        completion(nil, error)
                 //   }
                }
                return
            }
            
            let responseObject = response.result.value as? [String:AnyObject?]
            guard responseObject != nil else {
                
                let error = NSError(domain: unableToConnect, code: 404, userInfo: [NSLocalizedDescriptionKey : unableToConnect])
                
                completion(nil, error)
                return;
            }
            
            if (responseObject![ResponseKey.success.rawValue] as? [String : AnyObject]) != nil { // Successfully get response
                
                completion(responseObject![ResponseKey.success.rawValue] as AnyObject?, nil)
                
            } else if (responseObject![ResponseKey.error.rawValue] as? [String : AnyObject]) != nil { //server  error
                
                let responseData = Mapper<GeneralResponse>().map(JSON: responseObject![ResponseKey.error.rawValue]  as! [String : AnyObject])
                
//                if responseData?.code == errorCode.AuthorizationFailed.rawValue {
//                    Helper.popToLogin()
//                } else{
                
                var message = kEmptyString
                if(responseData?.messages != nil)
                {
                    let messagessObj = responseData!.messages
                    
                    var variableString = ""
                    for i in 0 ..< messagessObj!.count
                    {
                        let objectValue = messagessObj![i]
                        variableString += objectValue
                    }
                    
                    message = variableString
                }
                else
                {
                    message = kAPIErrorMessage
                }
                
                    let error =  NSError(domain: message,
                                         code: (responseData?.code)!,
                                         userInfo: [NSLocalizedDescriptionKey : message])
                    
                    completion(nil, error)
              //  }
                
            }
        }
    }
    
    
//    static func accessToken() {
//        
//        let loginUser = DataHandler.sharedInstance.getUser(userId: DataHandler.sharedInstance.loginId)
//        let accessToken = loginUser?.accessToken
//        
//        do {
//            let jwt = try decode(jwt: accessToken!)
//            let expireDate = Calendar.current.date(byAdding: .minute, value: -1, to: jwt.expiresAt!)
//            let localUTCDate = Helper.utcConversion(date: Date())
//            
//            if expireDate?.compare(localUTCDate) != ComparisonResult.orderedDescending {
//                let _ = ApiHelper.getRequest([:], kPath:GeneralPaths.accessToken.rawValue , completion: { (data, error) in
//                    
//                    if error != nil {
//                        
//                        let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
//                        let viewControllers: [UIViewController] = navigationController.viewControllers
//                        
//                        for aViewController in viewControllers {
//                            if aViewController is StartScreen {
//                                navigationController.popToViewController(aViewController, animated: true)
//                            }
//                            else if aViewController == viewControllers.last {
//                                let storyboard: UIStoryboard = UIStoryboard.newsFeedsStoryboard()
//                                let vc = storyboard.instantiateViewController(withIdentifier: START_SCREEN)
//                                navigationController.pushViewController(vc, animated: false)
//                            }
//                        }
//                        
//                    } else {
//                        
//                        let loginUser = DataHandler.sharedInstance.getUser(userId:DataHandler.sharedInstance.loginId )
//                        let realm = try! Realm()
//                        try! realm.write {
//                            loginUser?.accessToken = data?[ResponseKey.data.rawValue] as! String
//                        }
//                    }
//                    
//                })
//            }
//        }
//            
//            
//        catch {
//            print("Failed to decode JWT: \(error)")
//        }
//    }*/
    

 }
