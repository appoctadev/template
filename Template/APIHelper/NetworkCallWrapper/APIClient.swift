//
//  APIClient.swift
//  Template
//
//  Created by MacBook on 1/18/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit

import Alamofire
import AlamofireObjectMapper
import CoreLocation
import CoreTelephony
import SystemConfiguration

class APIClient: NSObject {


    static let shared = APIClient()
    
    var manager: SessionManager {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 5
        return manager
    }
    
    //FIXME: -  View Model
    func callAPI(url: URL, block:((_ responce: String?) -> Void)?)
    {
        if block != nil {
            block!("test")
        }
    }
    
    override init() {
        //intilizeAnimation()
    }

    //FIXME: -  Check Internet Connectivity
    static func isConnectedToNetwork() -> Bool {
        if Reachability.isConnectedToNetwork() == true {
            return true
        }
        else {
            return false
        }
    }

    static func checkAndShowAlert()
    {
        if(APIClient.isConnectedToNetwork() == false)
        {
            // show alert
            showAlert(title: "Error", message: "Kindly check your internet connection", viewController:  (appDelet.window?.rootViewController)!)
            return
        }
        else
        {
            intilizeAnimation()
            isShowIndicator(showIndica: true)
        }
    }
    
    //FIXME: -  GetLatLong
    static func geoCodeReverseAPI(urlString: String, block: ((_ response: GoogleMapsLatLngModel?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let URL = urlString
        
        APIClient.checkAndShowAlert()
        
        Alamofire.request(URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseObject { (response: DataResponse<GoogleMapsLatLngModel>) in
            
            var userObj: GoogleMapsLatLngModel?
            
            let baseResponce = response.result.value
            userObj = baseResponce
            
            isShowIndicator(showIndica: false)
            
            if block != nil {
                block!(userObj, nil, "Ok")
            }
        }
    }
    
    //FIXME: -  Logout API
    static func LogoutAPI(udid: String, block: ((_ responce: UserObject?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let URL = baseUrl+"\(kRemoveDeviceToken)"
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(Helper.getUser()!.accesToken!)"]
        
        let parameters: Parameters = [
            "udid" : udid,
            "type" : "ios"]
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<UserResponse>) in
            
            var error: Error?
            var message: String?
            var userObj: UserObject?
            
            isShowIndicator(showIndica: false)
            
            DataHandler.sharedInstance.user = nil
            Helper.removeUser()
            
            var appGroupDefaults = UserDefaults.standard
            appGroupDefaults = UserDefaults(suiteName:"group.com.cmolds.BusyBee")!
            appGroupDefaults.set("false", forKey: "isUserLogin")
            appGroupDefaults.synchronize()
            
            if let code = response.response?.statusCode
            {
                let baseResponce = response.result.value
                if code == 200 {
                    userObj = baseResponce?.response
                }else{
                    
                    let messagessObj = baseResponce?.error?.messages
                    
                    var variableString = ""
                    for i in 0 ..< messagessObj!.count
                    {
                        let objectValue = messagessObj![i]
                        variableString += objectValue
                    }
                    
                    message = variableString //baseResponce?.error?.messages![0]
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(userObj, error, message)
            }
        }
    }
    
    //FIXME: -  Register Device Token
    static func RegisterDeviceTokenRequest(udid: String, token: String, block: ((_ responce: UserObject?, _ error:Error?, _ message:String?) -> Void)?) {
        
        let URL = baseUrl+"\(kUpdateDeviceTokenJSON)"
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(Helper.getUser()!.accesToken!)"]
        
        let parameters: Parameters = [
            "udid" : udid,
            "token" : token,
            "type" : "ios"]
        
        Alamofire.request(URL, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<UserResponse>) in
            
            var error: Error?
            var message: String?
            var userObj: UserObject?
            
            isShowIndicator(showIndica: false)
            
            if let code = response.response?.statusCode {
                let baseResponce = response.result.value
                if code == 200 {
                    userObj = baseResponce?.response
                }else{
                    
                    let messagessObj = baseResponce?.error?.messages
                    
                    var variableString = ""
                    for i in 0 ..< messagessObj!.count
                    {
                        let objectValue = messagessObj![i]
                        variableString += objectValue
                    }
                    
                    message = variableString //baseResponce?.error?.messages![0]
                }
            }else{
                error = response.result.error
                message = error?.localizedDescription
            }
            
            if block != nil {
                block!(userObj, error, message)
            }
        }
    }
    
    //FIXME: -  Upload file
    static func sendReportAttachment(reportId: String, comment:  String, imagesObject1: UIImage, imagesObject2: UIImage,  imagesObject3: UIImage, imagesObject4: UIImage, imagesObject5: UIImage, block: ((_ response: GeneralResponse?, _ error:Error?, _ message:String?, _ code: Int?) -> Void)?) {
        
        let URL = baseUrl+"\(kUplaodCommentJSON)"
        
        let headers = ["Authorization" : "Bearer \(Helper.getUser()!.accesToken!)"]
        
        APIClient.checkAndShowAlert()
        
        var parametr = Parameters()
        parametr["id"] = reportId
        parametr["comment"] = comment
        
        var codeValue = 0
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            

            if let imageData = imagesObject1.jpegData(compressionQuality: 0.75)
            {
                multipartFormData.append(imageData, withName: "images[0]", fileName: "file.png", mimeType: "image/png")
                multipartFormData.append("png".data(using: .utf8)!, withName: "type")
            }

            for (key, value) in parametr {
                multipartFormData.append(((value as AnyObject).data(using: String.Encoding.utf8.rawValue))!, withName: key)
            }}, to: URL, method: .post, headers: headers,
                encodingCompletion: { encodingResult in
                    
                    var userObj: GeneralResponse?
                    var error: Error?
                    var message: String?
                    
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseObject(completionHandler: { (response: DataResponse<GeneralResponse>) in
                            printObject(objectPrint: response)
                            
                            isShowIndicator(showIndica: false)
                            
                            if let code = response.response?.statusCode {
                                let baseResponce = response.result.value
                                if code == 200
                                {
                                    codeValue = 200
                                    userObj = baseResponce
                                    let messagessObj = baseResponce?.messages
                                    
                                    if(messagessObj != nil)
                                    {
                                        var variableString = ""
                                        for i in 0 ..< messagessObj!.count
                                        {
                                            let objectValue = messagessObj![i]
                                            variableString += objectValue
                                        }
                                        message = variableString
                                    }
                                    else
                                    {
                                        message = kAPIErrorMessage
                                    }
                                    
                                }else{
                                    
                                    codeValue = 300
                                    let messagessObj = baseResponce?.messages
                                    
                                    if(messagessObj != nil)
                                    {
                                        var variableString = ""
                                        for i in 0 ..< messagessObj!.count
                                        {
                                            let objectValue = messagessObj![i]
                                            variableString += objectValue
                                        }
                                        message = variableString
                                    }
                                    else
                                    {
                                        message = kAPIErrorMessage
                                    }
                                }
                            }else{
                                codeValue = 300
                                error = response.result.error
                                message = error?.localizedDescription
                            }
                            
                            if block != nil {
                                block!(userObj, error, message, codeValue)
                            }
                            
                        })
                        
                    case .failure(let encodingError):
                        printObject(objectPrint:"error:\(encodingError)")
                        
                        
                        if block != nil {
                            block!(userObj, error, message, codeValue)
                        }
                        
                        break
                    }
        })
        
    }
}
