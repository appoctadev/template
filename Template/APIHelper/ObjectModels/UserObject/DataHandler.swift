//
//  DataHandler.swift
//  Template
//
//  Created by MacBook on 1/18/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit

import Foundation
import ObjectMapper
import GoogleMaps

class DataHandler {
    
    // MARK: - Shared Instance
    static let sharedInstance = DataHandler()
    
    var accessToken : String = ""
    var image : UIImage?
    var user : User?
    var driverLat : CLLocationDegrees = 0.0
    var driverLng : CLLocationDegrees = 0.0
    
    func getHeader(contentType:Bool)->[String:String]? {
        if DataHandler.sharedInstance.user?.accesToken != "" && DataHandler.sharedInstance.user?.accesToken != nil {
            let headers = contentType == true ? [
                "Authorization": "Bearer \((DataHandler.sharedInstance.user?.accesToken)!)",
                "Accept": "application/json"
                ]:["Authorization": "Bearer \((DataHandler.sharedInstance.user?.accesToken)!)"]
            printObject(objectPrint:"Headers : \(headers)")
            return headers
        }
        return ["Accept": "application/json"]
    }
    
    func mapUserResponse(responseData: AnyObject?) -> User? {
        let user = Mapper<User>().map(JSON: responseData as! [String : AnyObject])
        DataHandler.sharedInstance.user = user
        return user
    }
    
    func removeUser(){
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.removeObject(forKey: "vertified")
    }
}
