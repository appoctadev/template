//
//  User.swift
//  Template
//
//  Created by MacBook on 1/18/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import RMMapper

import ObjectMapper
class UserObject : RMMapper, Mappable
{
    var code: Int?
    var message: [String]?
    var data: User?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        code <- map["code"]
        message <- map["messages"]
        data <- map["data"]
    }
}


class User: RMMapper, Mappable
{
    var images: UserImg?
    var userId: String?
    var full_name: String?
    var username: String?
    var profession: String?
    var grade: String?
    var email: String?
    var accesToken: String?
    var subscription: String?
    var user_type: String?
    var signup_via: String?
    var imgUrl: String?
    
    var gender : String?
    var phone : String?
    
    var Braintree_token: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        //        user <- map["data"]
        images <- map["image_urls"]
        userId <- map["id"]
        full_name <- map["full_name"]
        username <- map["username"]
        email <- map["email"]
        grade <- map["grade"]
        profession <- map["discipline"]
        accesToken <- map["access_token"]
        subscription <- map["subscription"]
        user_type <- map["user_type"]
        signup_via <- map["signup_via"]
        Braintree_token <- map["Braintree_token"]
        
        gender <- map["gender"]
        phone <- map["phone"]
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let userID = aDecoder.decodeObject(forKey: "id") as? String
        let full_name = aDecoder.decodeObject(forKey: "full_name") as? String
        
        let username = aDecoder.decodeObject(forKey: "username") as? String
        let email = aDecoder.decodeObject(forKey: "email") as? String
        let accesstoken = aDecoder.decodeObject(forKey: "access_token") as? String
        let subscription = aDecoder.decodeObject(forKey: "subscription") as? String
        let user_type = aDecoder.decodeObject(forKey: "user_type") as? String
        let signup_via = aDecoder.decodeObject(forKey: "signup_via") as? String
        let imgUrl = aDecoder.decodeObject(forKey: "imgUrl") as? String
        let images = aDecoder.decodeObject(forKey: "images") as? UserImg
        let grade = aDecoder.decodeObject(forKey: "grade") as? String
        let profession = aDecoder.decodeObject(forKey: "profession") as? String
        let braintreetoken = aDecoder.decodeObject(forKey: "Braintree_token") as? String
        
        let gender = aDecoder.decodeObject(forKey: "gender") as? String
        let phone = aDecoder.decodeObject(forKey: "phone") as? String
        
        self.init(id: userID, username: username, email: email, accessToken: accesstoken, subscription: subscription, user_type: user_type, signup_via: signup_via, imgUrl: imgUrl, images: images, grade: grade, profession: profession, braintreetoken: braintreetoken ,gender: gender, phone : phone,full_name: full_name)
    }
    
    init(id: String?, username: String?, email: String?, accessToken: String?, subscription: String?, user_type: String?, signup_via: String?, imgUrl: String?, images: UserImg?, grade: String?, profession: String?, braintreetoken: String?, gender: String?, phone: String?,full_name:String?) {
        
        self.userId = id
        self.full_name = full_name
        self.email = email
        self.accesToken = accessToken
        self.subscription = subscription
        self.user_type = user_type
        self.signup_via = signup_via
        self.imgUrl = imgUrl
        self.username = username
        self.images = images
        self.grade = grade
        self.profession = profession
        self.Braintree_token = braintreetoken
        
        self.gender = gender
        self.phone = phone
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(userId, forKey: "id")
        aCoder.encode(full_name, forKey: "full_name")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(accesToken, forKey: "access_token")
        aCoder.encode(subscription, forKey: "subscription")
        aCoder.encode(user_type, forKey: "user_type")
        aCoder.encode(signup_via, forKey: "signup_via")
        aCoder.encode(imgUrl, forKey: "imgUrl")
        aCoder.encode(images, forKey: "images")
        aCoder.encode(grade, forKey: "grade")
        aCoder.encode(profession, forKey: "profession")
        aCoder.encode(Braintree_token, forKey: "Braintree_token")
        
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(phone, forKey: "phone")
    }
}
