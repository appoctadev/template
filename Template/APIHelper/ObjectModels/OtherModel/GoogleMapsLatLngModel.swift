//
//  GoogleMapsLatLngModel.swift
//  Template
//
//  Created by MacBook on 1/28/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit

import ObjectMapper
class GoogleMapsLatLngModel: NSObject, Mappable {
    
    var status: String?
    var resultArray: [GoogleMapsFormatAddress]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        resultArray <- map["results"]
    }
}

class GoogleMapsFormatAddress: NSObject, Mappable
{
    var formatted_address: String?
    var place_id: String?
    var geomery: GoogleMapsGeoMery?
    var address_components: [AddressComponnetObject]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        formatted_address <- map["formatted_address"]
        place_id <- map["place_id"]
        geomery <- map["geometry"]
        address_components <- map["address_components"]
    }
}

class AddressComponnetObject: NSObject, Mappable
{
    var long_name: String?
    var short_name: String?
    var types: [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        long_name <- map["long_name"]
        short_name <- map["short_name"]
        types <- map["types"]
    }
}

class GoogleMapsGeoMery: NSObject, Mappable{
    var locations: LocationObject??
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        locations <- map["location"]
    }
}

class LocationObject: NSObject, Mappable{
    var lat: Double?
    var lng: Double?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        lat <- map["lat"]
        lng <- map["lng"]
    }
}
