//
//  UserResponse.swift
//  Template
//
//  Created by MacBook on 1/18/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit


import UIKit
import ObjectMapper

class UserResponse: Mappable {
    var response: UserObject?
    var error: ErrorCase?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        response <- map["response"]
        error <- map["error"]
    }
}

class GeneralResponse: Mappable
{
    var code: Int?
    var messages: [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        messages <- map["messages"]
    }
}


class ForgetPasswordOM: Mappable {
    var response: ForgetPasswordResponseModel?
    var error : ErrorCase?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        response <- map["response"]
        error <- map["error"]
    }
}

class ForgetPasswordResponseModel : Mappable{
    
    var code: Int?
    var messages: [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        messages <- map["messages"]
    }
}


class ErrorCase: Mappable {
    var code: Int?
    var messages: [String]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        messages <- map["messages"]
    }
}

class UserImg: NSObject, Mappable, NSCoding {
    
    var x1: String?
    var x2: String?
    var x3: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        x1 <- map["1x"]
        x2 <- map["2x"]
        x3 <- map["3x"]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(x1, forKey: "1x")
        aCoder.encode(x2, forKey: "2x")
        aCoder.encode(x3, forKey: "3x")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let x1 = aDecoder.decodeObject(forKey: "1x") as? String
        let x2 = aDecoder.decodeObject(forKey: "2x") as? String
        let x3 = aDecoder.decodeObject(forKey: "3x") as? String
        
        self.init(x1: x1, x2: x2, x3: x3)
    }
    
    init(x1: String?, x2: String?, x3: String?) {
        self.x1 = x1
        self.x2 = x2
        self.x3 = x3
    }
    
}
