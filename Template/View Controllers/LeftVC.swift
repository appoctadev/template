//
//  LeftVC.swift
//  Template
//
//  Created by Nouman Gul on 1/15/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit
import LGSideMenuController

class LeftVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func showSegueViewController(_ sender: UIButton) {
        
        let navigationViewController = ((self.navigationController?.children[0] as! MainVC).children[1] as! UINavigationController)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        navigationViewController.pushViewController(vc, animated: true)
        
        sideMenuController?.hideLeftView(animated: false, completionHandler: {
        })
    }
    
    @IBAction func showSegueTabBarViewController(_ sender: UIButton) {
        
        let navigationViewController = ((sideMenuController?.children[1] as? UITabBarController)?.selectedViewController as? UINavigationController)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
        navigationViewController?.pushViewController(vc, animated: true)
        
        (sideMenuController?.children[1] as? UITabBarController)?.tabBar.isHidden = true
        sideMenuController?.hideLeftView(animated: false, completionHandler: {
        })
    }
    
    //MARK:- My Deinit Method
    deinit {
        printObject(objectPrint:"LeftVC deinit called")
    }
}
