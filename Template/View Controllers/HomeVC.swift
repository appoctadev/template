//
//  HomeVC.swift
//  Template
//
//  Created by Nouman Gul on 1/15/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import GoogleMaps
import UIKit
import Lottie

class HomeVC: UIViewController {

    var viewModel: NetworkViewModel?
    var userModel: User?
    
    var animatedValue = AnimationView(name: kEmptyString)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setBarLeftMenu()
        self.viewModel = NetworkViewModel.shared

        callingApi()

//        callingSocialNetwokignapi()
        
        
//        callingImagePicture()
        
        
//       let valud =  DataLoaderStrings.loadingData.localized
        
//       validation()
        
       // callcustomLoader()

    }
    
    func callcustomLoader()
    {
        
        //let animationView4 = AnimationView(name: "1124-loader");
        

        //animationView4.frame = CGRect(x: 0, y: 0, width: 200, height: 100)
        
        
        //animationView4.play()
        
        self.animatedValue.play { (finished) in
            /// Animation stopped
            
            if(finished)
            {
                self.callcustomLoader()
            }
        }
        
//        animationView4.play(fromMarker: "1124-loader", toMarker: "1124-loader")

        
        //animationView4.play(fromProgress: 0.5, toProgress: 1)

    }
    /*
     // MARK: - validation
     */
    
    func validation()
    {
        let testemail = "test"
        let email = "asdf"
        let phone = "123".isValidPassword
        let phonenumber = "123".isAlphanumeric
        let num = "123123123123"
        
        let password = "2123123"
        
        
        FormValidation.actionButtonformValidation(name: testemail, email: email, phoneNo: num, password: password) { (validForm, formatPhone) in
            
        }
    }
    
    /*
     // MARK: - Social Networking call
     */
    func callingImagePicture()
    {
        let customPIck = CustomGalleryPicker.shared
        customPIck.viewController = self
        customPIck.delegates = self
        customPIck.openPhotoLibrary()
    }
    

    func callingImagePost(mutablearr: NSMutableArray)
    {
        /*let mutablearr = NSMutableArray()
        
        for i in 0 ..< mutablearr.count
        {
            let imagesKeyDic = NSMutableDictionary()
            
            let keyValue = "key"+String(i)
            
            imagesKeyDic.setObject(keyValue, forKey: "keyValue" as NSCopying)
            imagesKeyDic.setObject(#imageLiteral(resourceName: "profile_pic_square_placeholder"), forKey: "image" as NSCopying)
            
            mutablearr.add(imagesKeyDic)
        }*/
        
        
        _ = ParameterKeysObject.createParameterOfImage(idValue: "QoZ2arLz0e", comment: "tesginv comment", imagesObject: mutablearr) { (handlerImagesArray, paramter) in
            
            DispatchQueue.main.async
            {
                self.viewModel?.setImageDataParamters(paremter: paramter!, urlPath: APiCallPath.kUploadMultipleImages.rawValue, postType: .uploadImages, imageDataArray: handlerImagesArray!)
                
                self.viewModel?.get { articleListViewModel in
                    
                  /*  if let model = articleListViewModel as? User
                    {
                        self.userModel = model
                    }*/
                }
            }
        }
    }
    
    /*
     // MARK: - Calling simple api
     */
    
    func callingApi()
    {
        let paramterObject = ParameterKeysObject.setupParamterObject(emailObject: "iosb@a.com", pasword: "1234567890", udid: "ios", apiCallType: APiCallPath.kApiLogin.rawValue)
        
        self.viewModel?.setParamter(paremter: paramterObject, urlPath: APiCallPath.kApiLogin.rawValue, postType: .postRequst)
        
        self.viewModel?.get { articleListViewModel in
            
            if let model = articleListViewModel as? User
            {
                self.userModel = model
            }
        }
    }
    
    /*
     // MARK: - Social Networking call
     */
    func callingSocialNetwokignapi()
    {
        let initSocial = SocialNetworkingCalls.shared
        initSocial.viewController = self
        initSocial.delegate = self
        
//        initSocial.twitterLogin()
//        initSocial.fbLogins()
//        initSocial.googleLogin()
//        initSocial.loginWithLinkedin()
    }
    
    /*
     // MARK: - Google Maps
   */
    
    func GoogleMapsIntegration()
    {
        let googleMapsObject = GoogleMapsWrapper.shared
        googleMapsObject.delegte = self
        googleMapsObject.actionButtonShowMap(viewCon: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- My Deinit Method
    deinit {
        printObject(objectPrint:"HomeVC deinit called")
    }
}


extension HomeVC : SocialNetworkingCompltionHandlerDelegate
{
    func sendFacebookData(singltn: SocialNetworkingSingleton?)
    {
        printObject(objectPrint: singltn as Any)
    }
    
    func sendTwitterData(singltn: SocialNetworkingSingleton?)
    {
        printObject(objectPrint: singltn as Any)
    }
    
    func sendGoogleData(singltn: SocialNetworkingSingleton?)
    {
        printObject(objectPrint: singltn as Any)
    }
    
    func sendLinkdeinObject(singltn: SocialNetworkingSingleton?)
    {
        printObject(objectPrint: singltn as Any)
    }
}


extension HomeVC : CustomGalleryPickerDelegate
{
    func sendImageSelection(image: [UIImage]?)
    {
        print("images")
        
        let imageCount = (image?.count ?? 0)
        
        let mutablearr = NSMutableArray()
        
        for i in 0 ..< imageCount
        {
            let imagesKeyDic = NSMutableDictionary()
            
            let keyValue = "key"+String(i)
            let img = image![i]
            
            imagesKeyDic.setObject(keyValue, forKey: "keyValue" as NSCopying)
            imagesKeyDic.setObject(img, forKey: "image" as NSCopying)
            
            mutablearr.add(imagesKeyDic)
        }
        
        self.callingImagePost(mutablearr: mutablearr)
    }
    
    func sendVideoSelction(videoURL: URL?)
    {
//        let mutablearr = NSMutableArray()
        
      /*  var imgData = Data.init()
        do{
            imgData = try Data.init(contentsOf: videoURL!)
        }
        catch{
        }
        
        */
    }
}


extension HomeVC: PlacesGoogleLocationDelegate
{
    /// Media Launched successfully on the cast device
    func getLaitude(latitude: String, longitdue: String, address: String, city: String, state: String, marker: GMSMarker?)
    {
        print("asdf")
    }
}
