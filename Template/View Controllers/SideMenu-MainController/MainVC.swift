//
//  MainVC.swift
//  TheMall
//
//  Created by APP on 1/22/18.
//  Copyright © 2018 APP. All rights reserved.
//

import UIKit
import LGSideMenuController

class MainVC: LGSideMenuController {
    
    var regularStyle: UIBlurEffect.Style? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupLeftAndRootView()
    }
    
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- My Helper Methods
   public func setupLeftAndRootView() {
        if #available(iOS 10.0, *) {
            regularStyle = .regular
        }
        else {
            regularStyle = .light
        }
        leftViewWidth = screenWidth-50
        rootViewCoverColorForLeftView = .clear
        leftViewPresentationStyle = .slideBelow
        leftViewLayerShadowRadius = 100.0
        leftViewLayerShadowColor = UIColor.black
        isLeftViewSwipeGestureEnabled = true
    }
    
    override func leftViewWillLayoutSubviews(with size: CGSize) {
        super.leftViewWillLayoutSubviews(with: size)
        
        if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
        }
    }
    
   
    //MARK:- My Deinit Method
    deinit {
        printObject(objectPrint:"MainVC deinit called")
    }
}



