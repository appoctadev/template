//
//  tabBarVC.swift
//  Template
//
//  Created by Nouman Gul on 1/16/19.
//  Copyright © 2019 Nouman Gul. All rights reserved.
//

import UIKit

class tabBarVC: UITabBarController {

    
}

extension tabBarVC {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tabBar.items![0].selectedImage = UIImage(named: "tab_01_active")?.withRenderingMode(.alwaysOriginal)
        self.tabBar.items![1].selectedImage = UIImage(named: "tab_02_active")?.withRenderingMode(.alwaysOriginal)
        self.tabBar.items![2].selectedImage = UIImage(named: "tab_03")?.withRenderingMode(.alwaysOriginal)
        self.tabBar.items![2].image = UIImage(named: "tab_03")?.withRenderingMode(.alwaysOriginal)
        self.tabBar.items![3].selectedImage = UIImage(named: "tab_04_active")?.withRenderingMode(.alwaysOriginal)
        self.tabBar.items![4].selectedImage = UIImage(named: "tab_05_active")?.withRenderingMode(.alwaysOriginal)
        self.tabBar.unselectedItemTintColor = UIColor.black
        
        self.delegate = self
        // Do any additional setup after loading the view.
    }
}

extension tabBarVC : UITabBarControllerDelegate{
  
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let index = tabBar.items?.index(of: item)
        let imageName = "tab_0\((index ?? 0) + 1)_active"
        self.tabBar.items![index!].selectedImage = UIImage(named: imageName )?.withRenderingMode(.alwaysOriginal)
    }
}
